#!/usr/bin/python

'''


TODO:
* check for valid chain cfg
* change in header?
'''
import sys
import os

def is_chainy(line):
    '''
    Return True if the passed PDB line contains chain id according to PDB format spec
    '''
    return line[0:6] in ('ATOM  ', 'HETATM', 'ANISOU', 'TER   ')

def parse_rename_cfg(rename_cfg):
    if ':' not in rename_cfg[0]:
        return rename_cfg[0]
    else:
        out = {}
        for item in rename_cfg:
            key, val = item.split(':')
            out[key] = val
        return out

def rename_chains(pdb_filename, rename_cfg):


    rename_cfg = parse_rename_cfg(rename_cfg)

    with open(pdb_filename) as f:
        out_lines = []
        if isinstance(rename_cfg, str):
            segid = rename_cfg
            if len(segid) > 4:
                print('ERROR: SEGIDs "{0}" longer than the maximum of 4 characters.'.format(segid))
                sys.exit()
            for line in f:
                if is_chainy(line):
                    cur_chain = line[21]
                    new_line = ''.join([line[:72] + segid + line[76:]])

                    out_lines.append(new_line)
                else:
                    out_lines.append(line)
        else:
            for line in f:
                if is_chainy(line):
                    try:
                        cur_chain = line[21]
                    except IndexError:
                        out_lines.append(line)
                    else:
                        if cur_chain in rename_cfg:
                            name = rename_cfg[cur_chain]
                            if len(name) > 4:
                                print('ERROR: SEGIDs "{0}" longer than the maximum of 4 characters.'.format(name))
                                sys.exit()
                            segid = "{0}".format(name).rjust(4)
                            new_line = ''.join([line[:72] + segid + line[76:]])
                            out_lines.append(new_line)
                        else:
                            out_lines.append(line)
                else:
                    out_lines.append(line)

 
        sys.stdout.write(''.join(out_lines)) #using sys.stdout.write because print would add extra end of the file


if __name__ == '__main__':
    if len(sys.argv) < 3:
        usage = '''Usage: {0} pdb_filename segid_cfg

Set SEGID for chains in PDB with no other changes to the PDB file.
Note!: SEGID can have maximum of 4 characters!


Doesn't uses any external parsers, just inserts SEGIDs, and nothing else.

Usage:
* Set segid of everything in chupacabra.pdb to XXX
{0} chupacabra.pdb XXX > chupacabra.A.pdb
* set segid of chain C to XXX and D to YYY:
{0} chupacabra.pdb C:XXX D:YYY > chupacabrbra2.pdb
* Invalid:
{0} chupacabra.pdb XXX YYY ZZZ
{0} chupacabra.pdb XXX C:YYY
        '''.format(os.path.basename(sys.argv[0]))
        print(usage)
        exit()
    pdb_filename = sys.argv[1]
    rename_cfg = sys.argv[2:]
    rename_chains(pdb_filename, rename_cfg)