#!/usr/bin/python

'''
Copy numbering from one PDB file to another.
'''
import sys
import os
from itertools import groupby

def is_coord_line(line):
    return line[0:6] in ('ATOM  ', 'HETATM', 'ANISOU', 'TER   ')

def coord_lines(f):
    for line in f:
        if is_coord_line(line):
            yield True, line
        else:
            yield False, line

def resi_grouper(sth):
    is_coord, line = sth
    if is_coord:
        chain = line[21]
        resi = line[22:26]
        marker = chain + resi
    else:
        marker = line
    
    return marker

def renum_line(line, new_num, chain=None):
    # print line
    cur_chain = line[21]
    # resi = line[22:26]
    if (chain is None) or (chain is not None and cur_chain == chain):

        new_line = ''.join([line[:22], str(new_num).rjust(4), line[26:]])
    else:
        new_line = line
    if new_line[-1] != '\n':
        new_line = new_line + '\n'
    # print new_line
    return new_line

def renum_resi(f1, f2):
    nums = []
    for k, lines in groupby(coord_lines(f2), resi_grouper):
        is_coord, line = next(lines)

        if is_coord:
            nums.append(line[22:26])
    print(len(nums))
    resi_count = 0
    out_lines = []
    for k, lines in groupby(coord_lines(f1), resi_grouper):
        is_coord, line = next(lines)

        if is_coord:

            out_lines.append(renum_line(line, nums[resi_count]))
            for is_coord, line in lines:
                out_lines.append(renum_line(line, nums[resi_count]))

            resi_count = resi_count + 1
        else:
            # print line
            out_lines.append(line)

    sys.stdout.write(''.join(out_lines)) #using sys.stdout.write because print would add extra end of the file

def main():
    if len(sys.argv) < 3 or sys.argv[1] in ('-h', '--help'):
        usage = '''Usage: {0} pdb_filename_to pdb_file_from > out.pdb

Copy numbering from one to another with no other changes to the PDB file.

Doesn't uses any external parsers, just locates and changes the chains, and nothing else.

        '''.format(os.path.basename(sys.argv[0]))
        print(usage)
        exit()
    pdb_filename1 = sys.argv[1]
    pdb_filename2 = sys.argv[2]

    with open(pdb_filename1) as f1:
        with open(pdb_filename2) as f2:
            renum_resi(f1, f2)

if __name__ == '__main__':
    main()