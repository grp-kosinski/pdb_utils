import os
import sys

if __name__ == '__main__':
    if len(sys.argv) < 2:
        usage = '''Usage: {0} pdb_filename > out_pdb_filename

Remove HELIX and SHEET records
        '''.format(os.path.basename(sys.argv[0]))
        print(usage)
        exit()
    pdb_filename = sys.argv[1]

    out_lines = []
    for line in open(pdb_filename):
        if not line.startswith('SHEET') and not line.startswith('HELIX'):
            out_lines.append(line)

    sys.stdout.write(''.join(out_lines))  # using sys.stdout.write because print would add extra end of the file