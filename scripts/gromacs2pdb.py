#!/usr/bin/python

import sys
import os

def is_coord_line(line):
    return line[0:6] in ('ATOM  ', 'HETATM', 'ANISOU', 'TER   ')

rename = {
    ''
}

if __name__ == '__main__':
    if len(sys.argv) < 2:
        usage = '''Usage: {0} pdb_filename > outpdbfilename'''.format(os.path.basename(sys.argv[0]))
        print(usage)
        exit()

    pdb_filename = sys.argv[1]
    out_lines = []
    with open(pdb_filename) as f:
        for line in f:
            if is_coord_line(line):
                resi_name = line[17:20].strip()
                atom_name = line[12:16].strip()
                if resi_name == 'ILE':
                    if atom_name == 'CD':
                        line = line[:12] + ' CD1' + line[16:]
                if atom_name == 'OC1':
                    line = line[:12] + ' O  ' + line[16:]
                if atom_name == 'OC2':
                    line = line[:12] + ' OXT' + line[16:]

            out_lines.append(line)

    print(''.join(out_lines))
