#!/usr/bin/python

'''

'''
import sys
import os

def is_coord_line(line):
    return line[0:6] in ('ATOM  ', 'HETATM', 'ANISOU', 'TER   ')

def coord_lines(f):
    for line in f:
        if is_coord_line(line):
            yield True, line
        else:
            yield False, line

resn_map = {
    'U': 'URI',
    'A': 'ADE',
    'C': 'CYT',
    'G': 'GUA'
}

def rename_resi(line):
    resn = line[17:20]
    new_resn = resn_map[resn.strip()]
 
    return line[:17] + new_resn.rjust(3) + line[20:]

phosph_map = {
    'OP1': 'O1P',
    'OP2': 'O2P',
    'OP3': 'O3P'
}
def rename_phosp(line):
    atomn = line[12:16].strip()
    if atomn in phosph_map:
        new_resn = phosph_map[atomn]
 
        return line[:12] + new_resn.rjust(4) + line[16:]
    else:
        return line

def pdb2cns(f):
    out_lines = []
    for line in f:
        if is_coord_line(line):
            out_lines.append(rename_phosp(rename_resi(line)))
        else:
            out_lines.append(line)

    sys.stdout.write(''.join(out_lines)) #using sys.stdout.write because print would add extra end of the file


if __name__ == '__main__':
    if len(sys.argv) < 1:
        usage = '''Usage: {0} pdb_filename > out_pdbfilename
        '''.format(os.path.basename(sys.argv[0]))
        print(usage)
        exit()
    pdb_filename = sys.argv[1]

    with open(pdb_filename) as f:
        pdb2cns(f)