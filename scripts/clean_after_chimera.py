import os
import sys

from itertools import tee, izip
def pairwise(iterable):
    "s -> (s0,s1), (s1,s2), (s2, s3), ..."
    a, b = tee(iterable)
    next(b, None)
    return izip(a, b)

def remove_extra_TERs(lines):
    out_lines = []
    for curr_line, next_line in pairwise(lines):
        if is_chainy(curr_line):
            curr_chain = curr_line[21]
            if is_chainy(next_line):
                next_chain = next_line[21]
            else:
                next_chain = None
            if curr_line.startswith('TER') and next_chain == curr_chain:
                continue
            out_lines.append(curr_line)

            if not curr_line.startswith('TER') and next_chain != curr_chain:
                new_TER_line = curr_line[:26] + '\n'
                new_TER_line = new_TER_line[:12] + '    ' + new_TER_line[16:]
                new_TER_line = 'TER   ' + new_TER_line[6:]
                out_lines.append(new_TER_line)
        else:
            out_lines.append(curr_line)

        if next_line.startswith('END'):
            out_lines.append(next_line)

    return out_lines

def fix_atom_numbers_pdb(lines):
    out_lines = []
    i = 1
    for line in lines:
        if is_chainy(line):
        # if is_atom(line):
            atom_num = line[6:11]
            line = line[:6] + str(i).rjust(5) + line[11:]

            i = i + 1

        out_lines.append(line)

    return out_lines

def is_chainy(line):
    '''
    Return True if the passed PDB line contains chain id according to PDB format spec
    '''
    return line[0:6] in ('ATOM  ', 'HETATM', 'ANISOU', 'TER   ')


if __name__ == '__main__':
    if len(sys.argv) < 2:
        usage = '''Usage: {0} pdb_filename > out_pdb_filename

Remove HELIX and SHEET records
        '''.format(os.path.basename(sys.argv[0]))
        print(usage)
        exit()
    pdb_filename = sys.argv[1]

    out_lines = []
    for line in open(pdb_filename):
        if not line.startswith('SHEET') and not line.startswith('HELIX') and not line.startswith('CONECT'):
            out_lines.append(line)


    out_lines = remove_extra_TERs(out_lines)
    out_lines = fix_atom_numbers_pdb(out_lines)

    sys.stdout.write(''.join(out_lines))  # using sys.stdout.write because print would add extra end of the file