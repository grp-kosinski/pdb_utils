#!/usr/bin/python

import sys
import os

def is_coord_line(line):
    return line[0:6] in ('ATOM  ', 'HETATM', 'ANISOU', 'TER   ')

rename = {
    ''
}

if __name__ == '__main__':
    if len(sys.argv) < 2:
        usage = '''Usage: {0} pdb_filename > outpdbfilename'''.format(os.path.basename(sys.argv[0]))
        print(usage)
        exit()

    pdb_filename = sys.argv[1]
    out_lines = []
    with open(pdb_filename) as f:
        for line in f:
            if line.startswith('TER'):
                atom_and_space = line[6:12]
                # print(line)
                # print('|{0}|'.format(atom_and_space))
                # print('|{0}|'.format(atom_and_space.strip().ljust(6))) 
                line = line[:6] + atom_and_space.strip().ljust(6) + line[12:]
            else:
                out_lines.append(line)

    print(''.join(out_lines))
