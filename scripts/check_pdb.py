#!/usr/bin/python

import sys
import os

def is_coord_line(line):
    return line[0:6] in ('ATOM  ', 'HETATM', 'ANISOU', 'TER   ')

if __name__ == '__main__':
    if len(sys.argv) < 2:
        usage = '''Usage: {0} pdb_filename '''.format(os.path.basename(sys.argv[0]))
        print(usage)
        exit()

    pdb_filename = sys.argv[1]
    out_lines = []
    is_hetatm_first = False
    is_atom_first = False

    already_warned = []
    with open(pdb_filename) as f:
        for line in f:
            if is_coord_line(line):
                resi_name = line[17:20].strip()
                atom_name = line[12:16].strip()
                chain = line[21]
                resi = line[22:26]

                if line.startswith('ATOM') and is_hetatm_first == False:
                    is_atom_first = True
                if line.startswith('HETATM') and is_atom_first == False:
                    is_hetatm_first = True

                if is_hetatm_first:
                    out = 'HETATM lines preceed ATOM lines'
                    if out not in already_warned:
                        already_warned.append(out)
                        print(out)

                if resi_name == 'ILE':
                    if atom_name == 'CD':
                        print('CD atom instead of CD1 for {0}.{1}'.format(resi, chain))
                if atom_name == 'OC1':
                    print('OC1 atom instead of O for {0}.{1}'.format(resi, chain))
                if atom_name == 'OC2':
                    print('OC2 atom instead of OXT for {0}.{1}'.format(resi, chain))

                