#!/usr/bin/python

'''
Get residue ranges covered by the passed PDB file
'''
import sys
import os
from itertools import groupby


def is_coord_line(line):
    return line[0:6] in ('ATOM  ', 'HETATM', 'ANISOU', 'TER   ')

def coord_lines(f):
    for line in f:
        if is_coord_line(line):
            yield True, line
        else:
            yield False, line

def resi_grouper(sth):
    is_coord, line = sth
    if is_coord:
        chain = line[21]
        resi = line[22:26]
        marker = chain + resi
    else:
        marker = line
    
    return marker

def chain_grouper(sth):
    is_coord, line = sth
    if is_coord:
        marker = line[21]
    else:
        marker = line
    
    return marker

def ranges_from_list(i):
    # https://stackoverflow.com/questions/4628333/converting-a-list-of-integers-into-range-in-python
    for a, b in groupby(enumerate(i), lambda pair: pair[1] - pair[0]):
        b = list(b)
        yield b[0][1], b[-1][1]

def get_coverage(f):
    for chain_id, chain_lines in groupby(coord_lines(f), chain_grouper):
        resi_nums = []
        for k, lines in groupby(chain_lines, resi_grouper):
            is_coord, line = next(lines)

            if is_coord:
                resi_nums.append(int(line[22:26]))

        for first, second in ranges_from_list(resi_nums):
            print(chain_id, first, second)


def main():
    pdb_filename = sys.argv[1]

    with open(pdb_filename) as f:
        get_coverage(f)

if __name__ == '__main__':
    main()