#!/usr/bin/python

import sys
import os

def is_atom_line(line):
    return line[0:6] in ('ATOM  ', 'HETATM', 'ANISOU')

rename = {
    ''
}

if __name__ == '__main__':
    if len(sys.argv) < 2:
        usage = '''Usage: {0} pdb_filename occupancy > outpdbfilename'''.format(os.path.basename(sys.argv[0]))
        print(usage)
        exit()

    pdb_filename = sys.argv[1]
    occupancy = "{0:.2f}".format(float(sys.argv[2])).rjust(6)
    out_lines = []
    with open(pdb_filename) as f:
        for line in f:
            if is_atom_line(line):
               line = line[:54] + occupancy + line[60:]

            out_lines.append(line)

    print(''.join(out_lines))
