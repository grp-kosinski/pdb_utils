#!/usr/bin/python

import sys
import os

def is_coord_line(line):
    return line[0:6] in ('ATOM  ', 'HETATM', 'ANISOU', 'TER   ')

def is_hetatm(line):
    '''
    Return True if the passed PDB line contains chain id according to PDB format spec
    '''
    return line[0:6] == 'HETATM'

if __name__ == '__main__':
    if len(sys.argv) < 2:
        usage = '''Usage: {0} pdb_filename'''.format(os.path.basename(sys.argv[0]))
        print(usage)
        exit()

    pdb_filename = sys.argv[1]
    out_lines = []
    with open(pdb_filename) as f:
        at_i = 1
        prev_chain = None
        for line in f:
            if is_coord_line(line):
                cur_chain = line[21]
                # if cur_chain != prev_chain:
                #     at_i = 1
                line = line[:6] + str(at_i).rjust(5) + line[11:]
                at_i = at_i + 1
                prev_chain = cur_chain
            out_lines.append(line)

    print(''.join(out_lines))
