#!/usr/bin/python

'''
Rename residues in PDB with no minimal changes to the PDB file.
'''
import sys
import os
from itertools import groupby


def fasta_iter(fh):
    """
    given a fasta file. yield tuples of header, sequence
    """
    # ditch the boolean (x[0]) and just keep the header or sequence since
    # we know they alternate.
    faiter = (x[1] for x in groupby(fh, lambda line: line[0] == ">"))
    for header in faiter:
        # drop the ">"
        header = next(header)[1:].strip()
        # join all sequence lines to one.
        seq = "".join(s.strip() for s in next(faiter))
        yield header, seq

class TooFewSequences(Exception): pass
def seqs_to_cols(aln_seqs):
    if len(aln_seqs) < 2:
        raise TooFewSequences('Provide at least two sequences as input')

    return list(zip(*aln_seqs))

class ModelSequenceNotFound(Exception): pass
def get_aln_cols(aln_filename, pdbfilename):
    aln_seqs = []
    pdbfileshortname = os.path.basename(pdbfilename)
    with open(aln_filename) as f:
        parser = fasta_iter(f)
        name, seq = next(parser) #first is ref seq
        aln_seqs.append(seq)
        for name, seq in parser:
            if name == pdbfileshortname:
                aln_seqs.append(seq)
                break
    
    if len(aln_seqs) < 2:
        raise ModelSequenceNotFound('Seq with name "{0}" not found in {1}'.format(pdbfilename, aln_filename))

    return seqs_to_cols(aln_seqs)

class SequenceNumNotTwo(Exception): pass
def aln_cols_to_num_map(aln_cols):
    a_i = 1
    b_i = 1
    num_map = {}
    for a, b in aln_cols:
        if '-' not in (a, b):
            num_map[b_i] = a_i
        if a != '-':
            a_i = a_i + 1
        if b != '-':
            b_i = b_i + 1

    return num_map

def is_coord_line(line):
    return line[0:6] in ('ATOM  ', 'HETATM', 'ANISOU', 'TER   ')

def coord_lines(f):
    for line in f:
        if is_coord_line(line):
            yield True, line
        else:
            yield False, line

def resi_grouper(sth):
    is_coord, line = sth
    if is_coord:
        chain = line[21]
        resi = line[22:26]
        marker = chain + resi
    else:
        marker = line
    
    return marker

def renum_line(line, new_num, chain=None):
    # print line
    cur_chain = line[21]
    # resi = line[22:26]
    if (chain is None) or (chain is not None and cur_chain == chain):

        new_line = ''.join([line[:22], str(new_num).rjust(4), line[26:]])
    else:
        new_line = line
    if new_line[-1] != '\n':
        new_line = new_line + '\n'
    # print new_line
    return new_line

def renum_resi(f, num_map, chain=None):
    '''
    num_map - dict mapping current residue index to new
    '''
    resi_count = 1
    out_lines = []
    for k, lines in groupby(coord_lines(f), resi_grouper):
        is_coord, line = next(lines)

        if is_coord:
            new_num = num_map[resi_count]
            out_lines.append(renum_line(line, new_num, chain))
            for is_coord, line in lines:
                out_lines.append(renum_line(line, new_num, chain))

            resi_count = resi_count + 1
        else:
            # print line
            out_lines.append(line)

    sys.stdout.write(''.join(out_lines)) #using sys.stdout.write because print would add extra end of the file

def main():
    pdb_filename = sys.argv[1]
    aln_filename = sys.argv[2]


    aln_cols = get_aln_cols(aln_filename, pdb_filename)
    num_map = aln_cols_to_num_map(aln_cols)

    with open(pdb_filename) as f:
        renum_resi(f, num_map, chain=None)

if __name__ == '__main__':
    main()