#!/usr/bin/python

'''


TODO:
* check for valid chain cfg
* change in header?
'''
import sys
import os

def is_model_line(line):
    '''
    Return True if the passed PDB line contains chain id according to PDB format spec
    '''
    return line[0:6] in ('MODEL ',)



def split_models(pdb_filename):
    sort_order = []
    with open(pdb_filename) as f:
        out_lines = {}
        cur_model = None
        for line in f:
            if is_model_line(line):
                cur_model = line[6:].strip()
                if cur_model not in out_lines:
                    out_lines[cur_model] = [line]
                    sort_order.append(cur_model)
            elif cur_model is not None:
                out_lines[cur_model].append(line)
        print() 
        root, ext = os.path.splitext(pdb_filename)
        for model_id in sort_order:

            outfilename = root + '.' + model_id + ext
            with open(outfilename, 'w') as f:
                f.write(''.join(out_lines[model_id]))



if __name__ == '__main__':
    if len(sys.argv) < 2:
        usage = '''Usage: {0} pdb_filename
Split chains into individual files
        '''.format(os.path.basename(sys.argv[0]))
        print(usage)
        exit()
    pdb_filename = sys.argv[1]

    split_models(pdb_filename)