import sys
from collections import defaultdict
import itertools
import os

def split_chains(pdb_filename):

    with open(pdb_filename) as f:
        out_lines = {}

        for line in f:
            if is_atom(line) or is_hetatm(line):
                cur_chain = line[21]
                if cur_chain not in out_lines:
                    out_lines[cur_chain] = [line]
                else:
                    out_lines[cur_chain].append(line)

        return out_lines

def is_atom(line):
    '''
    Return True if the passed PDB line contains chain id according to PDB format spec
    '''
    return line[0:6] in ('ATOM  ', '  ATOM')

def is_hetatm(line):
    '''
    Return True if the passed PDB line contains chain id according to PDB format spec
    '''
    return line[0:6] == 'HETATM'

def resi_grouper(sth):
    is_coord, line = sth
    if is_coord:
        chain = line[21]
        resi = line[22:26]
        marker = chain + resi
    else:
        marker = line
    
    return marker

def is_coord_line(line):
    return line[0:6] in ('ATOM  ', 'HETATM', 'ANISOU', 'TER   ')

def coord_lines(f):
    for line in f:
        if is_coord_line(line):
            yield True, line
        else:
            yield False, line

def make_pdb_lines(pdb_filenames):
    if len(pdb_filenames) > 0:
        grouped = defaultdict(list)
        for pdb_filename in pdb_filenames:
            splitted = split_chains(pdb_filename)
            for chain, lines in splitted.items():
                grouped[chain].append(lines)

        out_lines = []
        for chain_id in sorted(grouped):
            all_chain_lines = itertools.chain(*grouped[chain_id])
            grouped_by_resi = itertools.groupby(coord_lines(all_chain_lines), resi_grouper)
            
            grouped_by_resi1 = []
            for k, lines in grouped_by_resi:
                grouped_by_resi1.append((k, list([y for x,y in lines])))

            lines = []
            for k, resi_lines in sorted(grouped_by_resi1, key=lambda the_tuple: int(the_tuple[0][1:])): #[1:] because resi_grouper returns chain+resi
                lines.extend(resi_lines)

            # sorted_frags = sorted(grouped[chain_id], key=lambda lines: int(lines[0][22:26]))

            # lines = list(itertools.chain(*sorted_by_resi))

            out_lines.extend(lines)
            out_lines.append('TER\n')

        # out_lines.append('END\n')
        out_lines = fix_atom_numbers_pdb(out_lines)

        return out_lines

def fix_atom_numbers_pdb(lines):
    out_lines = []
    i = 1
    for line in lines:
        if is_chainy(line):
        # if is_atom(line):
            atom_num = line[6:11]
            line = line[:6] + str(i).rjust(5) + line[11:]

            i = i + 1

        out_lines.append(line)

    return out_lines

def is_chainy(line):
    '''
    Return True if the passed PDB line contains chain id according to PDB format spec
    '''
    return line[0:6] in ('ATOM  ', 'HETATM', 'ANISOU', 'TER   ')

def fix_TER(lines):
    prev = None
    out = []
    for line in lines:
        if line[:3] == 'TER':
            line = prev[:26] + '\n'
            line = line[:12] + '    ' + line[16:]
            line = 'TER   ' + line[6:]
        else:
            prev = line
        out.append(line)

    return out

if __name__ == '__main__':
    if len(sys.argv) < 2:
        usage = '''Usage: {0} pdb_filename1 pdb_filename2 ...
        '''.format(os.path.basename(sys.argv[0]))
        print(usage)
        exit()
    pdb_filenames = sys.argv[1:]


    all_pdb_lines = []

    pdb_lines = make_pdb_lines(pdb_filenames)
    all_pdb_lines.extend(pdb_lines)

    all_pdb_lines = fix_TER(all_pdb_lines)
    all_pdb_lines = fix_atom_numbers_pdb(all_pdb_lines)

    print(''.join(all_pdb_lines))