import sys
import re

def is_atom(line):
    '''
    Return True if the passed PDB line contains chain id according to PDB format spec
    '''
    items = line.split()
    if len(items) > 0 and items[0] == 'ATOM':
        return True
    # return line[0:6].strip() in ('ATOM  ', '  ATOM')

def is_hetatm(line):
    return line.startswith('HETATM')

def get_Calpha(filename):
    out = []
    with open(filename) as f:
        for cif_line in f:
            if is_atom(cif_line):
                items = re.split('(\s+)', cif_line)
                atom = items[6]
                if atom == 'CA':
                    out.append(cif_line)
            elif not is_hetatm(cif_line):
                out.append(cif_line)

    return out

if __name__ == '__main__':
    if len(sys.argv) < 2:
        usage = '''Usage: {0} cif_filename
Split chains into individual files
        '''.format(os.path.basename(sys.argv[0]))
        print(usage)
        exit()
    cif_filename = sys.argv[1]

    print(''.join(get_Calpha(cif_filename)))