#!/usr/bin/python

'''


TODO:
* check for valid chain cfg
* change in header?
'''
import sys
import os
from optparse import OptionParser

def is_coord_line(line):
    '''
    Return True if the passed PDB line contains chain id according to PDB format spec
    '''
    return line.startswith(('ATOM', 'HETATM', 'ANISOU', 'TER'))

def parse_rename_cfg(rename_cfg):
    if ':' not in rename_cfg[0]:
        return rename_cfg[0]
    else:
        out = {}
        for item in rename_cfg:
            key, val = item.split(':')
            out[key] = val
        return out

def get_cif_chain_id(line, auth_asym_id_idx):
    return line.split()[auth_asym_id_idx].strip()

def get_pdb_chain_id(line):
    return line[21].strip()

def replace_cif_chain_id(line, new_chain_id, cif_atom_keys):
    line_dict = dict(zip(cif_atom_keys, line.split()))
    line_dict['_atom_site.label_asym_id'] = new_chain_id
    line_dict['_atom_site.auth_asym_id'] = new_chain_id
    outline = '  '.join([line_dict[key] for key in cif_atom_keys]) + '\n' #yes, double spacing, TODO: nicely format
    
    return outline

def replace_pdb_chain_id(line, new_chain_id):
    return ''.join([line[:21], new_chain_id, line[22:]])

def rename_chains(filename, rename_cfg, is_cif=False):
    rename_cfg = parse_rename_cfg(rename_cfg)

    with open(filename) as f:
        if is_cif:
            header = []
            cif_coord_lines = []
            tail = []
            cif_atom_keys = []
            for line in f:
                if is_coord_line(line):
                    cif_coord_lines.append(line)
                elif len(cif_coord_lines) == 0:
                    header.append(line)
                    if line.startswith('_atom_site.'):
                        cif_atom_keys.append(line.strip())
                else:
                    tail.append(line)

            print(cif_atom_keys)

            auth_asym_id_idx = cif_atom_keys.index('_atom_site.auth_asym_id')
            label_asym_id_idx = cif_atom_keys.index('_atom_site.label_asym_id')

            get_chain_id = lambda line: get_cif_chain_id(line, auth_asym_id_idx)
            replace_chain_id = lambda line, new_chain_id: replace_cif_chain_id(line, new_chain_id, cif_atom_keys)
            lines = header + cif_coord_lines + tail
        else:
            get_chain_id = get_pdb_chain_id
            replace_chain_id = replace_pdb_chain_id
            lines = f

        out_lines = []
        if isinstance(rename_cfg, str):
            new_chain = rename_cfg
            for line in lines:
                if is_coord_line(line):
                    cur_chain = get_chain_id(line)
                    new_line = replace_chain_id(line, new_chain)

                    out_lines.append(new_line)
                else:
                    out_lines.append(line)
        else:
            for line in lines:
                if is_coord_line(line):
                    try:
                        cur_chain = get_chain_id(line)
                    except IndexError:
                        out_lines.append(line)
                    else:
                        if cur_chain in rename_cfg:
                            new_chain = rename_cfg[cur_chain]
                            new_line = replace_chain_id(line, new_chain)
                            out_lines.append(new_line)
                        else:
                            out_lines.append(line)
                else:
                    out_lines.append(line)

 
        sys.stdout.write(''.join(out_lines)) #using sys.stdout.write because print would add extra end of the file


if __name__ == '__main__':
    usage = '''Usage:  %prog [options] pdb_filename rename_cfg

Rename chains in PDB with no other changes to the PDB file.

Doesn't uses any external parsers, just locates and changes the chains, and nothing else.

Usage:
* Set chain of everything in chupacabra.pdb to A
python %prog chupacabra.pdb A > chupacabra.A.pdb
* rename chain C to A and D to B:
python %prog chupacabra.pdb C:A D:B > chupacabra2.pdb
python %prog --cif chupacabra.cif A:A1 B:B1 > chupacabra2.cif
* Invalid:
python %prog chupacabra.pdb A B C
python %prog chupacabra.pdb A C:B
        '''
    parser = OptionParser(usage=usage)

    parser.add_option("--cif", dest="is_cif", action="store_true", default=False,
                      help="Input is cif file")

    (options, args) = parser.parse_args()

    if len(args) < 2:
        parser.print_usage()
        exit()
    filename = args[0]
    rename_cfg = args[1:]

    rename_chains(filename, rename_cfg, options.is_cif)


