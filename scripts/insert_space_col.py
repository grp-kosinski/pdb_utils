#!/usr/bin/python

'''
Rename residues in PDB with no minimal changes to the PDB file.
'''
import sys
import os


def is_coord_line(line):
    return line[0:6] in ('ATOM  ', 'HETATM', 'ANISOU', 'TER   ')


def coord_lines(f):
    for line in f:
        if is_coord_line(line):
            yield True, line
        else:
            yield False, line


def main():
    if len(sys.argv) < 2:
        usage = '''Usage: {0} pdb_filename col_idx > out_pdb_filename
Insert a space column after a column with given idx (counting from 1)
        '''.format(os.path.basename(sys.argv[0]))
        print(usage)
        exit()
    pdb_filename = sys.argv[1]
    idx = int(sys.argv[2])

    out_lines = []
    with open(pdb_filename) as f:
        for is_coord, line in coord_lines(f):
            if is_coord:
                out_lines.append(line[:idx] + ' ' + line[idx:])
            else:
                out_lines.append(line)

        sys.stdout.write(''.join(out_lines))  # using sys.stdout.write because print would add extra end of the file

if __name__ == '__main__':
    main()
