#!/usr/bin/python

'''


TODO:
* check for valid chain cfg
* change in header?
'''
import sys
import os

def is_chainy(line):
    '''
    Return True if the passed PDB line contains chain id according to PDB format spec
    '''
    return line[0:6] in ('ATOM  ', 'HETATM', 'ANISOU', 'TER   ')



def split_chains(pdb_filename):

    with open(pdb_filename) as f:
        out_lines = {}

        for line in f:
            if is_chainy(line):
                cur_chain = line[21]
                if cur_chain not in out_lines:
                    out_lines[cur_chain] = [line]
                else:
                    out_lines[cur_chain].append(line)

        root, ext = os.path.splitext(pdb_filename)
        for chain in out_lines:

            outfilename = root + '.' + chain + ext
            with open(outfilename, 'w') as f:
                f.write(''.join(out_lines[chain]))



if __name__ == '__main__':
    if len(sys.argv) < 2:
        usage = '''Usage: {0} pdb_filename
Split chains into individual files
        '''.format(os.path.basename(sys.argv[0]))
        print(usage)
        exit()
    pdb_filename = sys.argv[1]

    split_chains(pdb_filename)