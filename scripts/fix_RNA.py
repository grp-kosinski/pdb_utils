#!/usr/bin/python

import sys
import os

def is_atom(line):
    '''
    Return True if the passed PDB line contains chain id according to PDB format spec
    '''
    return line[0:6] in ('ATOM  ', '  ATOM')

def is_hetatm(line):
    '''
    Return True if the passed PDB line contains chain id according to PDB format spec
    '''
    return line[0:6] == 'HETATM'


def fix_TER(lines):
    prev = None
    out = []
    for line in lines:
        if line[:3] == 'TER':
            line = prev[:26] + '\n'
            line = line[:12] + '   ' + line[15:]
            line = 'TER   ' + line[6:]
        else:
            prev = line
        out.append(line)

    return out


two_to_one = {
    'RA': 'A',
    'RG': 'G',
    'RU': 'U',
    'RC': 'C'
}

def main():
    pdb_filename = sys.argv[1]

    out_lines = []
    with open(pdb_filename) as f:
        for line in f:
            if is_atom(line) or is_hetatm(line):
                # print line[12:16]
                if line[12:16] == ' O1P':
                    line = line[:12] + ' OP1' + line[16:]
                if line[12:16] == ' O2P':
                    line = line[:12] + ' OP2' + line[16:]
                resn = line[17:20].strip()
                if resn in ('A', 'U', 'G', 'C'):
                    line = line[:17] + '  ' + resn + line[20:]
                if resn in list(two_to_one.keys()):
                    newresn = two_to_one[resn]
                    line = line[:17] + ' ' + newresn + line[20:]

            out_lines.append(line)

    out_lines = fix_TER(out_lines)
    
    print(''.join(out_lines))


if __name__ == '__main__':
    main()