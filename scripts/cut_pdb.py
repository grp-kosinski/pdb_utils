#!/usr/bin/python

import sys
import os
import re
from itertools import groupby

if sys.version_info[0] == 2:
    range = xrange

def parse_cut_cfg(cut_cfg):
    ranges = []
    for range_cfg in cut_cfg:
        m = re.match('(.):(.*)-(.*)', range_cfg)
        if m:
            chain, start, end = m.groups()
            ranges.append([chain, (int(start), int(end))])
        else:
            m = re.match('(.*)-(.*)', range_cfg)
            if m:
                start, end = m.groups()
                ranges.append([None, (int(start), int(end))])
            else:
                m = re.match('(.):', range_cfg)
                chain = m.groups()[0]
                ranges.append([chain, None])
    
    return ranges

def resi_grouper(sth):
    is_coord, line = sth
    if is_coord:
        chain = line[21]
        resi = line[22:26]
        marker = chain + resi
    else:
        marker = line
    
    return marker
def is_coord_line(line):
    return line[0:6] in ('ATOM  ', 'HETATM', 'ANISOU', 'TER   ')

def coord_lines(f):
    for line in f:
        if is_coord_line(line):
            yield True, line
        else:
            yield False, line


def cut_pdb(pdb_filename, cut_cfg):
    ranges = parse_cut_cfg(cut_cfg)
    out_lines = []
    with open(pdb_filename) as f:
        for k, lines in groupby(coord_lines(f), resi_grouper):
            is_coord, line = next(lines)

            if is_coord:
                cur_chain = line[21]
                cur_resid = int(line[22:26].strip())
                for chain_resi_range in ranges:
                    chain = chain_resi_range[0]
                    resi_range = chain_resi_range[1]
                    if resi_range is not None:
                        if cur_chain == chain or chain is None:
                            first_resi = resi_range[0]
                            last_resi = resi_range[1]
                            if cur_resid in range(first_resi, last_resi+1):
                                out_lines.append(line)
                                for is_coord, line in lines:
                                    out_lines.append(line)
                    else:
                        if cur_chain == chain:
                            out_lines.append(line)
                            for is_coord, line in lines:
                                out_lines.append(line)

    sys.stdout.write(''.join(out_lines)) #using sys.stdout.write because print would add extra end of the file

if __name__ == '__main__':
    if len(sys.argv) < 3:
        usage = '''Usage: {0} pdb_filename cut_cfg

Doesn't uses any external parsers, just locates and changes the chains, and nothing else.

Usage:
 * {0} chupacabra.pdb A:20-100 B:20-100
 * {0} chupacabra.pdb 20-100 20-100
 * {0} chupacabra.pdb 20-100
        '''.format(os.path.basename(sys.argv[0]))
        print(usage)
        exit()
    pdb_filename = sys.argv[1]
    cut_cfg = sys.argv[2:]
    cut_pdb(pdb_filename, cut_cfg)