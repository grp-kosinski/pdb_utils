#!/usr/bin/python

import sys
import os

def is_coord_line(line):
    return line[0:6] in ('ATOM  ', 'HETATM', 'ANISOU', 'TER   ')

def fix_TER(lines):
    prev = None
    out = []
    for line in lines:
        if line[:3] == 'TER':
            line = prev[:26] + '\n'
            line = line[:12] + '    ' + line[16:]
            line = 'TER   ' + line[6:]
        else:
            prev = line
        out.append(line)

    return out

if __name__ == '__main__':
    if len(sys.argv) < 2:
        usage = '''Usage: {0} pdb_filename > outfilename'''.format(os.path.basename(sys.argv[0]))
        usage += '\n'
        usage += 'Insert TERs on chain breaks'
        print(usage)
        exit()

    pdb_filename = sys.argv[1]
    out_lines = []
    with open(pdb_filename) as f:
        prev_resi = None
        prev_line = ''
        for line in f:
            if is_coord_line(line):
                cur_resi = int(line[22:26])
                if (prev_resi is not None) and (cur_resi - prev_resi > 1) and not prev_line.startswith('TER'):
                    out_lines.append('TER\n')
                prev_resi = cur_resi
            prev_line = line
            out_lines.append(line)

    print(''.join(out_lines))
