#!/usr/bin/python

'''
Check if the sequence in the PDB file matches a sequence in FASTA file
'''
import sys
import os
from itertools import groupby

def fasta_iter(fh):
    """
    given a fasta handle. yield tuples of header, sequence
    """
    # ditch the boolean (x[0]) and just keep the header or sequence since
    # we know they alternate.
    faiter = (x[1] for x in groupby(fh, lambda line: line[0] == ">"))
    for header in faiter:
        # drop the ">"
        header = next(header)[1:].strip()
        # join all sequence lines to one.
        seq = "".join(s.strip() for s in next(faiter))
        seq = seq
        yield header, seq

def is_coord_line(line):
    return line[0:6] in ('ATOM  ', 'HETATM', 'ANISOU', 'TER   ')

def coord_lines(f):
    for line in f:
        if is_coord_line(line):
            yield True, line
        else:
            yield False, line

def resi_grouper(sth):
    is_coord, line = sth
    if is_coord:
        chain = line[21]
        resi = line[22:26]
        marker = chain + resi
    else:
        marker = line

    return marker

def renum_line(line, new_num, chain=None):
    # print line
    cur_chain = line[21]

    # resi = line[22:26]
    if (chain is None) or (chain is not None and cur_chain == chain):

        new_line = ''.join([line[:22], str(new_num).rjust(4), line[26:]])
    else:
        new_line = line
    if new_line[-1] != '\n':
        new_line = new_line + '\n'
    # print new_line
    return new_line



def check_seq(f, seq, chain=None):
    '''
    num_map - dict mapping current residue index to new
    '''

    aa_3to1 = {'CYS': 'C', 'ASP': 'D', 'SER': 'S', 'GLN': 'Q', 'LYS': 'K',
         'ILE': 'I', 'PRO': 'P', 'THR': 'T', 'PHE': 'F', 'ASN': 'N', 
         'GLY': 'G', 'HIS': 'H', 'LEU': 'L', 'ARG': 'R', 'TRP': 'W', 
         'ALA': 'A', 'VAL':'V', 'GLU': 'E', 'TYR': 'Y', 'MET': 'M'}

    cur_chain = None
    chain_found = False
    for k, lines in groupby(coord_lines(f), resi_grouper):
        is_coord, line = next(lines)

        if is_coord:
            cur_chain = line[21]
            if cur_chain == chain or chain is None:
                chain_found = True
                resi_num = int(line[22:26])
                resn = aa_3to1[line[17:20].strip()]
                if resn != seq[resi_num-1]:
                    print("Wrong sequence")
                    print("PDB residue " + str(resi_num) + " is "+resn)
                    print("Fasta residue " + str(resi_num) + " is "+seq[resi_num-1])
                    sys.exit()

    if chain_found:
        print("Sequence OK!")
    else:
        print("Chain not found!")

def main():
    pdb_filename = sys.argv[1]
    fasta_fn = sys.argv[2]
    if len(sys.argv) > 3:
        chain = sys.argv[3]
    else:
        chain = None

    with open(fasta_fn) as f:
        seq = list(fasta_iter(f))[0][1]

    with open(pdb_filename) as f:
        check_seq(f, seq, chain)

if __name__ == '__main__':
    main()
