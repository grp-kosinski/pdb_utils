#!/usr/bin/python

'''
Rename residues in PDB with no minimal changes to the PDB file.
'''
import sys
import os
from itertools import groupby


def is_coord_line(line):
    return line[0:6] in ('ATOM  ', 'HETATM', 'ANISOU', 'TER   ')

def coord_lines(f):
    for line in f:
        if is_coord_line(line):
            yield True, line
        else:
            yield False, line

def resi_grouper(sth):
    is_coord, line = sth
    if is_coord:
        chain = line[21]
        resi = line[22:26]
        marker = chain + resi
    else:
        marker = line
    
    return marker

def get_missing(f, chain):
    resi_nums = []
    for k, lines in groupby(coord_lines(f), resi_grouper):
        is_coord, line = next(lines)

        if is_coord:
            resi_nums.append(int(line[22:26]))

    if resi_nums[0] > 1:
        print(1, resi_nums[0] - 1)
        
    for first, second in zip(resi_nums, resi_nums[1:]):
        if second - first > 1:
            print(first+1, second-1)

def main():
    pdb_filename = sys.argv[1]

    with open(pdb_filename) as f:
        get_missing(f, chain=None)

if __name__ == '__main__':
    main()