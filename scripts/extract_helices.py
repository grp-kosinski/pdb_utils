import ntpath
import os
import subprocess
import tempfile
import sys

import dssp

pdbfilename = sys.argv[1]
outdir = sys.argv[2]

tempfilehandle, dsspfilename = tempfile.mkstemp()
os.close(tempfilehandle)

try:
    subprocess.call('dssp {0} > {1}'.format(pdbfilename, dsspfilename), shell=True)

    print(dsspfilename)

    ss_ranges = dssp.get_DSSP_as_HE_ranges(dsspfilename)

finally:
    os.remove(dsspfilename)

helix_i = 0
for r in ss_ranges:
    
    if r['ss_type'] == 'H':
        print(r['start'], r['end'])

        suffix = '_H{0}.pdb'.format(helix_i)
        outfile = os.path.join(outdir, ntpath.basename(pdbfilename).replace('.pdb', suffix))

        scriptdir = os.path.dirname(os.path.realpath(__file__))
        cmd = 'python {scriptdir}/cut_pdb.py {pdb} {chain}:{start}-{end} > {outfile}'.format(
            pdb=pdbfilename,
            chain=r['chain'],
            start=r['start'],
            end=r['end'] + 1,
            outfile=outfile,
            scriptdir=scriptdir
            )
        print(cmd)
        subprocess.call(cmd, shell=True)
        helix_i = helix_i + 1