#!/usr/bin/python

'''


TODO:
* check for valid chain cfg
* change in header?
'''
import sys
import os

def is_end_line(line):
    '''
    Return True if the passed PDB line contains chain id according to PDB format spec
    '''
    return line[0:3] in ('END',)



def split_models(pdb_filename):
    root, ext = os.path.splitext(pdb_filename)
    with open(pdb_filename) as f:
        cur_model = 0
        out_lines = []
        for line in f:
            if is_end_line(line):
                out_lines.append(line)
                outfilename = root + '.' + str(cur_model) + ext
                with open(outfilename, 'w') as f:
                    f.write(''.join(out_lines))
                cur_model = cur_model + 1
                out_lines = []
            else:
                out_lines.append(line)





if __name__ == '__main__':
    if len(sys.argv) < 2:
        usage = '''Usage: {0} pdb_filename
Split chains into individual files
        '''.format(os.path.basename(sys.argv[0]))
        print(usage)
        exit()
    pdb_filename = sys.argv[1]

    split_models(pdb_filename)