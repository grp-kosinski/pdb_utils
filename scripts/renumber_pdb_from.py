#!/usr/bin/python

'''
Rename residues in PDB with no minimal changes to the PDB file.
'''
import sys
import os
from itertools import groupby

def is_coord_line(line):
    return line[0:6] in ('ATOM  ', 'HETATM', 'ANISOU', 'TER   ')

def coord_lines(f):
    for line in f:
        if is_coord_line(line):
            yield True, line
        else:
            yield False, line

def resi_grouper(sth):
    is_coord, line = sth
    if is_coord:
        chain = line[21]
        resi = line[22:26]
        marker = chain + resi
    else:
        marker = line

    return marker

def renum_line(line, new_num, chain=None):
    # print line
    cur_chain = line[21]

    # resi = line[22:26]
    if (chain is None) or (chain is not None and cur_chain == chain):

        new_line = ''.join([line[:22], str(new_num).rjust(4), line[26:]])
    else:
        new_line = line
    if new_line[-1] != '\n':
        new_line = new_line + '\n'
    # print new_line
    return new_line

def renum_resi(f, start=1, chain=None):
    '''
    num_map - dict mapping current residue index to new
    '''
    resi_count = start
    out_lines = []
    cur_chain = None

    for k, lines in groupby(coord_lines(f), resi_grouper):
        is_coord, line = next(lines)

        if is_coord:
            cur_chain = line[21]
            if cur_chain == chain:
                out_lines.append(renum_line(line, resi_count, chain))
                for is_coord, line in lines:
                    out_lines.append(renum_line(line, resi_count, chain))

                resi_count = resi_count + 1
            else:
                out_lines.append(line)
                for line in lines:
                    out_lines.append(line[1])
        else:
            # print line
            out_lines.append(line)

    sys.stdout.write(''.join(out_lines)) #using sys.stdout.write because print would add extra end of the file

def main():
    pdb_filename = sys.argv[1]
    start = int(sys.argv[2])
    chain = sys.argv[-1]

    with open(pdb_filename) as f:
        renum_resi(f, start, chain)

if __name__ == '__main__':
    main()
