#!/usr/bin/python

import sys
import os
import re
from itertools import groupby

def is_coord_line(line):
    return line[0:6] in ('ATOM  ', 'HETATM', 'ANISOU', 'TER   ')

# def is_not_hydrogen(line):
#     return is_coord_line(line) and not line[12:16].strip().startswith('H')

def is_not_hydrogen(line):
    return is_coord_line(line) and line[13] != 'H'

def filter_lines(pdb_filename, is_condition):
    out_lines = []
    with open(pdb_filename) as f:
        for line in f:
            if is_condition(line):
                out_lines.append(line)

    sys.stdout.write(''.join(out_lines)) #using sys.stdout.write because print would add extra end of the file

if __name__ == '__main__':
    if len(sys.argv) < 2:
        usage = '''Usage: {0} pdb_filename cut_cfg

Remove hydrogens
        '''.format(os.path.basename(sys.argv[0]))
        print(usage)
        exit()
    pdb_filename = sys.argv[1]
    cut_cfg = sys.argv[2:]
    filter_lines(pdb_filename, is_not_hydrogen)