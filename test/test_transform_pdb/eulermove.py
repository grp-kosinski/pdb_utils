# Script to rotate a volume using Euler angles, then shift it, then resample it
# on the original unrotated grid.
#
#    runscript eulermove.py #0 45 60 20 15 33.5 -10

if len(arguments) != 7:
    from Commands import CommandError
    raise CommandError('eulermove.py requires 7 arguments')

from chimera import specifier
m = specifier.evalSpec(arguments[0]).models()[0]
euler_angles = [float(a) for a in arguments[1:4]]
translation = [float(x) for x in arguments[4:7]]

from Matrix import euler_xform
xf = euler_xform(euler_angles, translation)
# mcopy = m.copy()
m.openState.localXform(xf)

from chimera import runCommand
runCommand('vop resample %s onGrid %s boundingGrid true' %
           (m.oslIdent(), m.oslIdent()))
