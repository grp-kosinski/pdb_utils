import unittest

import transform_pdb

class test_transform_pdb(unittest.TestCase):
    def test_tranform(self):
        rot = [[-0.037084, -0.983558, 0.176743],[-0.023889, 0.177686, 0.983797],[-0.999027, 0.032261, -0.030086]]
        trans = [0.090, 67.385, -2.190]
        transform_pdb.transform('./pdb3kdk.ent', rot, trans)

    def transform_from_quaternion(self):
        rot_quaternion = [0.353553, -0.612372, 0.612372, 0.353553]
        trans = [369.103, 254.856, 414.87]
        rot = transform_pdb.quaternion_to_matrix(rot_quaternion)

        print(transform_pdb.transform('./for_quaternion_ori.pdb', rot, trans))

    def transform_from_euler(self):
        rot_euler = [36.565, 137.807, 33.189]
        # rot_euler = [33.189, 137.807, 36.565]
        ori_center = [57.139, 113.606, 0.040]
        # new_center = [17.124, -71.507, 25.958]
        new_center = [321.624, 239.993, 316.458]

        trans = [new_center[0]-ori_center[0],
                new_center[1]-ori_center[1],
                new_center[2]-ori_center[2]
                ]
        rot_euler = [0, 0, 0]
        # trans = [-57.139, -113.606, -0.040]
        # trans = [0, 0, 0]
        trans = new_center

        rot = transform_pdb.euler_to_matrix(*rot_euler, in_degrees=True)
        # print transform_pdb.transform('./for_euler_ori.pdb', rot, trans)
        # print transform_pdb.transform('./centered.pdb', rot, trans)
        print(transform_pdb.transform('./rotated.pdb', rot, trans))
        # for k in transform_pdb._AXES2TUPLE.keys():
        #     rot_conv = 3.14159265358979323846/180
        #     psi = rot_euler[0] * rot_conv
        #     theta = rot_euler[1] * rot_conv
        #     phi = rot_euler[2] * rot_conv
        #     rot_quaternion = transform_pdb.quaternion_from_euler(phi, theta, psi, axes=k)
        #     # rot = transform_pdb.quaternion_to_matrix([rot_quaternion[3],rot_quaternion[0],rot_quaternion[1],rot_quaternion[2]])
        #     rot = transform_pdb.quaternion_to_matrix(rot_quaternion)
        #     outfile = 'out'+k+'.pdb'
        #     with open(outfile,'w') as f:
        #         f.write(transform_pdb.transform('./for_euler_ori.pdb', rot, trans))

if __name__ == '__main__':
    unittest.main()