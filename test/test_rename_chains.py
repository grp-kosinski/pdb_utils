import unittest
import subprocess
import tempfile

def is_coord_line(line):
    '''
    Return True if the passed PDB line contains chain id according to PDB format spec
    '''
    return line.startswith(('ATOM', 'HETATM', 'ANISOU', 'TER'))


class _Test_rename_chains_script(object):
    def test_rename_chains_4XMM(self):
        out_fn = tempfile.mkstemp()[1]
        print(out_fn)
        cmd = 'python ../scripts/rename_chains.py {2} {0} A:X B:Y > {1}'.format(self.infile, out_fn, self.options)
        print(cmd)
        subprocess.call(cmd, shell=True)

        lines = []
        with open(out_fn) as f:
            for line in f:
                if is_coord_line(line) and ' X ' in line:
                    lines.append(line)

        self.assertEqual(self.expected_X, len(lines))

        lines = []
        with open(out_fn) as f:
            for line in f:
                if is_coord_line(line) and ' Y ' in line:
                    lines.append(line)

        self.assertEqual(self.expected_Y, len(lines))

class Test_rename_chains_script_PDB(_Test_rename_chains_script, unittest.TestCase):
    def setUp(self):
        self.infile = '4XMM.pdb'
        self.options = ''
        self.expected_X = 2161 #one more than in CIF because of the TER 
        self.expected_Y = 3806 #one more than in CIF because of the TER 

class Test_rename_chains_script_CIF(_Test_rename_chains_script, unittest.TestCase):
    def setUp(self):
        self.infile = '4XMM.cif'
        self.options = '--cif'
        self.expected_X = 2160
        self.expected_Y = 3805

if __name__ == '__main__':
    unittest.main()
