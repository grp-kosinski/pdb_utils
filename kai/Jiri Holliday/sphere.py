import bpy
import bmesh
import math
import numpy.linalg as la
from collections import defaultdict
import numpy as np
import scipy.interpolate


color_dict = dict()
#color_dict["darkgrey"] = (0.4, 0.4, 0.4)
color_dict["red"] = (1.0, 0.0, 0.0) 
color_dict["green"] = (0.0, 1.0, 0.0)
color_dict["blue"] = (0.0, 0.0, 1.0)
color_dict["cyan"] = (0.0, 1.0, 1.0)
color_dict["magenta"] = (1.0, 0.0, 1.0)
color_dict["yellow"] = (1.0, 1.0, 0.0)
color_dict["white"] = (1.0, 1.0, 1.0)
#color_dict["black"] = (0.0, 0.0, 0.0)
color_dict["darkred"] = (0.5, 0.0, 0.0)
color_dict["darkgreen"] = (0.0, 0.5, 0.0)
color_dict["darkblue"] = (0.0, 0.0, 0.5)
color_dict["darkcyan"] = (0.0, 0.5, 0.5)
color_dict["darkmagenta"] = (0.5, 0.0, 0.5)
color_dict["darkyellow"] = (0.5, 0.5, 0.0)
#color_dict["lightgrey"] = (0.8, 0.8, 0.8)

colors = list(color_dict.keys())
mat_dict = dict()
alpha = lambda v: math.acos(v[2]/la.norm(v))
beta = lambda v: -math.atan(v[0]/v[1])
get_coords = lambda l:np.array([float(el) for el in l[31:54].split()])
get_chain = lambda l:l[21]
get_residue = lambda l:int(l[22:26].strip())
is_atomic = lambda l: any([s in l for s in ["ATOM","HETATM"]])
get_spec = lambda l: l[13:16]
n = la.norm

for k,v in color_dict.items():
    mat = bpy.data.materials.new(k)
    mat.diffuse_color = tuple(list(v))
    mat_dict[k] = mat


def sphere(pos,radius,color):
    bpy.ops.mesh.primitive_uv_sphere_add(radius=radius,location=tuple(pos))
    bpy.context.selected_objects[0].active_material = mat_dict[color]

def append_chains(pos_d,k1,k2):
    ret = []
    ret.extend([el for el in pos_d if el['chain']==k1])
    ret.extend([el for el in pos_d if el['chain']==k2])
    return ret

def get_pos_d(lines):
    pos_d = []
    res_l = []
    n_dict = None
    for i,l in enumerate(lines):
        if is_atomic(l):
            spec = l[13:16]
            res = get_residue(l)
            chain = get_chain(l)
            if not (chain,res) in res_l:
                if n_dict is not None:
                    pos_d.append(n_dict)
                n_dict = dict()
                n_dict['chain'] = chain
                n_dict['residue'] = res
                res_l.append((chain,res))
            if 'O3' in spec:
                n_dict['O3'] =  np.array(get_coords(l))
            elif 'OP1' in spec:
                n_dict['OP1'] = np.array(get_coords(l))
            elif 'OP2' in spec:
                n_dict['OP2'] = np.array(get_coords(l))
            elif 'P ' in spec:
                n_dict['P'] = np.array(get_coords(l))
            elif 'O5' in spec:
                n_dict['O5'] = np.array(get_coords(l))
            elif 'C5\'' in spec:
                n_dict['C5'] = np.array(get_coords(l))
            elif 'C4\'' in spec:
                n_dict['C4'] = np.array(get_coords(l))
            elif 'C3\'' in spec:
                n_dict['C3'] = np.array(get_coords(l))
    for d in pos_d:
        if all([k in d for  k in ['P','OP1','OP2','O3','O5']]):
            d['PO3'] = d['O3'] - d['P']
            d['PO5'] = d['O5'] - d['P']
            d['POP1'] = d['OP1'] - d['P']
            d['POP2'] = d['OP2'] - d['P']
    return pos_d

def get_x_d_s(pos_p,k='P'):
    x_d = []
    for i in range(len(pos_p)-1):
        p_i = pos_p[i][k]
        p_i_1 = pos_p[i+1][k]
        d = math.sqrt(sum([(e1-e0)**2 for e0,e1 in zip(p_i,p_i_1)]))
        x_d.append(d)
    x_d_s = [sum(x_d[:i]) for i in range(0,len(x_d)+1)]
    return x_d_s,x_d

def produce_splines_alt(pos_d,k1,k2):
    spline_dict = dict()
    pos = append_chains(pos_d,k1,k2)
    for data in ['P','O3','O5','OP1','OP2']:
        x_d_s,x_d = get_x_d_s(pos,data)
        eval_x = sum(x_d_s[x_d.index(max(x_d)):x_d.index(max(x_d))+2])/2
        spline_dict['eval'+data] = eval_x
        data_pos = [d[data] for d in pos]
        spline_dict[k1+k2+data] = scipy.interpolate.CubicSpline(x_d_s,data_pos)
        spline_dict['xds'+k] = x_d_s
        spline_dict['xd'+k] = x_d
    return spline_dict


scn = bpy.context.scene

coord_E_J = [ 87.86136708, 106.1081981 , 101.58814473]
coord_F_K = [109.28392868,  88.83227938, 100.98471716]
coord_I_H = [109.04143043, 129.57377542, 100.63968791]
coord_G_L = [130.31922946, 112.30710638, 101.43790934]


extra_coords = [coord_E_J,coord_F_K,coord_I_H,coord_G_L]
extra_vs = [[ 0.04988128,  0.90259269, -1.50955993],
            [-1.19390859, -0.57616617, -1.14431159],
            [ 1.26297609,  0.55373933, -1.10137291],
            [ 0.08339191, -0.923248  , -1.46801009]]

fn = '/Users/Kai/git/pdb_utils/kai/Jiri Holliday/HJ_DNA_Long_raw.pdb'

fh = open(fn)
gs = []
lines = [l for l in fh]

pos_d = get_pos_d(lines)
o3s = [get_coords(l) for l in lines if is_atomic(l) and 'O3' in get_spec(l)]

geo_d = []
for d in pos_d:
    if 'P' in d:
        for o in o3s:
            if n(d['P'] - o)<2:
                d['O3'] = o
                geo_d.append(d)

print(len(geo_d))
import sys
print(sys.version)
#produce_splines_alt(geo_d,'E','J')
#for i,d in enumerate(geo_d):
#    print("Processing object nr:",i)
#    for k in d.keys():
#        if k == 'O3':
#            sphere(d[k],0.5,'red')
#        elif k in ['O5','OP1','OP2']:
#            sphere(d[k],0.5,'yellow')
#        elif k == 'P':
#            sphere(d[k],1,'green')
#        

#chain_dict = defaultdict(list)
#for pl in plines:
#    chain_dict[get_chain(pl)].append((get_residue(pl),get_coords(pl)))

#chain_to_mat = dict()

#cc = 0
#for k in chain_dict.keys():
#    chain_to_mat[k] = mat_dict[colors[cc]]
#    cc+=1

#chain_to_mat["F"] = chain_to_mat["K"]
#chain_to_mat["I"] = chain_to_mat["H"]
#chain_to_mat["E"] = chain_to_mat["J"]
#chain_to_mat["G"] = chain_to_mat["L"]
#            


#for i in range(len(vs)):
#    n_arrow = arrow.copy()
#    n_arrow.data = arrow.data.copy()
#    n_arrow.animation_data_clear()
#    bpy.context.collection.objects.link(n_arrow)
#    n_arrow.location = tuple(posn[i][0])
#    n_arrow.rotation_euler = (alpha(vs[i][0]),0,beta(vs[i][0]))
#    
#for i in range(len(extra_vs)):
#    n_arrow = arrow.copy()
#    n_arrow.data = arrow.data.copy()
#    n_arrow.animation_data_clear()
#    bpy.context.collection.objects.link(n_arrow)
#    n_arrow.location = tuple(extra_coords[i])
#    n_arrow.active_material = mat_dict["green"]
#    n_arrow.rotation_euler = (alpha(extra_vs[i]),0,beta(extra_vs[i]))

#for e_c in extra_coords:
#    bpy.ops.mesh.primitive_uv_sphere_add(radius=1,location=tuple(e_c))
#    bpy.context.selected_objects[0].active_material = mat_dict["green"]