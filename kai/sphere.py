# import bpy
# import bmesh
import sys
#sys.path.insert(1, '/Library/Frameworks/Python.framework/Versions/3.8/lib/python3.8/site-packages')
import matplotlib.pyplot as plt
import math
import numpy.linalg as la
from collections import defaultdict
import numpy as np
from scipy.interpolate import CubicSpline
import scipy.optimize as opt

# color_dict = dict()
#color_dict["darkgrey"] = (0.4, 0.4, 0.4)
# color_dict["red"] = (1.0, 0.0, 0.0) 
# color_dict["green"] = (0.0, 1.0, 0.0)
# color_dict["blue"] = (0.0, 0.0, 1.0)
# color_dict["cyan"] = (0.0, 1.0, 1.0)
# color_dict["magenta"] = (1.0, 0.0, 1.0)
# color_dict["yellow"] = (1.0, 1.0, 0.0)
# color_dict["white"] = (1.0, 1.0, 1.0)
#color_dict["black"] = (0.0, 0.0, 0.0)
# color_dict["darkred"] = (0.5, 0.0, 0.0)
# color_dict["darkgreen"] = (0.0, 0.5, 0.0)
# color_dict["darkblue"] = (0.0, 0.0, 0.5)
# color_dict["darkcyan"] = (0.0, 0.5, 0.5)
# color_dict["darkmagenta"] = (0.5, 0.0, 0.5)
# color_dict["darkyellow"] = (0.5, 0.5, 0.0)
#color_dict["lightgrey"] = (0.8, 0.8, 0.8)

# colors = list(color_dict.keys())
# mat_dict = dict()
# alpha = lambda v: math.acos(v[2]/la.norm(v))
# beta = lambda v: -math.atan(v[0]/v[1])
# get_coords = lambda l:np.array([float(el) for el in l[31:54].split()])
# get_chain = lambda l:l[21]
# get_residue = lambda l:int(l[22:26].strip())
# is_atomic = lambda l: any([s in l for s in ["ATOM","HETATM"]])
# get_spec = lambda l: l[13:16]
# n = la.norm

# for k,v in color_dict.items():
#     mat = bpy.data.materials.new(k)
#     mat.diffuse_color = tuple(list(v)+[1.0])
#     mat_dict[k] = mat


#from scipy.spatial.transform import Rotation as R

def rmsd(p_list,q_list):
    return np.sqrt(np.sum([la.norm((p-q))**2 for p,q in zip(p_list,q_list)]))

def center(_list):
    p_center = _list[0]
    centered = np.array([e-p_center for e in _list])
    return 1/la.norm(centered)*centered

def random_rot():
    u1,u2,u3 = np.random.rand(3)
    q = (np.sqrt(1-u1)*np.sin(2*np.pi*u2),np.sqrt(1-u1)*np.cos(2*np.pi*u2),
        np.sqrt(u1)*np.sin(2*np.pi*u3),np.sqrt(u1)*np.cos(2*np.pi*u3))
    return R.from_quat(q)

def align(p_list,q_list):
        # p_list = np.array([[1,0,0],[0,1,0],[0,0,1],[0,0,1]])
        # q_list = np.array([[0,1,0],[-1,0,0],[0,0,1],[0,0,1]])
        p_list = center(p_list)
        q_list = center(q_list)
        # print(q_list)
        old = rmsd(p_list,q_list)
        M = np.zeros((4,4))
        for p,q in zip(p_list,q_list):
            d = p[0]**2 + p[1]**2 + p[2]**2 + q[0]**2 + q[1]**2 + q[2]**2
            M[0,0] += d - 2 * p[0] * q[0] - 2 * p[1] * q[1] - 2 * p[2] * q[2]
            M[0,1] += 2 * (p[1] * q[2] - p[2] * q[1])
            M[0,2] += 2 * (-p[0] * q[2] + p[2] * q[0])
            M[0,3] += 2 * (p[0] * q[1] - p[1] * q[0])
            M[1,1] += d - 2 * p[0] * q[0] + 2 * p[1] * q[1] + 2 * p[2] * q[2]
            M[1,2] += -2 * (p[0] * q[1] + p[1] * q[0])
            M[1,3] += -2 * (p[0] * q[2] + p[2] * q[0])
            M[2,2] += d + 2 * p[0] * q[0] - 2 * p[1] * q[1] + 2 * p[2] * q[2]
            M[2,3] += -2 * (p[1] * q[2] + p[2] * q[1])
            M[3,3] += d + 2 * p[0] * q[0] + 2 * p[1] * q[1] - 2 * p[2] * q[2]
        M[1,0] = M[0,1]
        M[2,0] = M[0,2]
        M[3,0] = M[0,3]
        M[2,1] = M[1,2]
        M[3,1] = M[1,3]
        M[3,2] = M[2,3]
        # print("==========")
        # print(M)
        w,v = la.eig(M)
        # print(p_list)        
        # print(w)
        # print(v)
        min_v = v[:,np.where(w==np.min(w))].flatten()
        # print(min_v)
        swap = min_v[0]
        min_v[0] = min_v[1]
        min_v[1] = min_v[2]
        min_v[2] = min_v[3]
        min_v[3] = swap
        r = R.from_quat(min_v)
        # print(min_v)
        # print(r.as_matrix())
        for i in range(p_list.shape[0]):
            p_list[i,:] = np.dot(p_list[i,:],r.as_matrix())
        # print(old, "-->", rmsd(p_list,q_list))
        # print(p_list)
        # print(q_list)
        # print("Base: ==============")
        # orig = rmsd(p_list,q_list)
        # print(orig)
        # for j in range(10000):
        #     r = random_rot()
        #     for i in range(p_list.shape[0]):
        #         p_list[i,:] = np.dot(p_list[i,:],r.as_matrix())
        #     n_rmsd = rmsd(p_list,q_list)
            # print("%i: %f %f %s"%(j,orig,n_rmsd,str(orig<=n_rmsd)))
        return p_list


def sphere(pos,radius,color):
    bpy.ops.mesh.primitive_uv_sphere_add(radius=radius,location=tuple(pos))
    bpy.context.selected_objects[0].active_material = mat_dict[color]


class PDBLine(object):
    def __init__(self,i=-1,_rawstring="",recordType=None,_next=None,_previous=None):
        self._raw = _rawstring
        self._i = i
        if recordType is None:
            self.recordType = PDBLine.getRecordType(_rawstring)
            #defaults
            if _next is not None:
                self._next = _next
            if _previous is not None:
                self._previous =  _previous
            if PDBLine.conectRecord(self.recordType):
                self.serials = PDBLine.getConectSerials(_rawstring)
            if PDBLine.terRecord(self.recordType) or PDBLine.atomRecord(self.recordType):
                self.serials = [PDBLine.getSerial(_rawstring)]
                self.residueName = PDBLine.getResidueName(_rawstring)
                self.chainId = PDBLine.getChainId(_rawstring)
                self.residueNumber = PDBLine.getResidueNumber(_rawstring)
            if PDBLine.atomRecord(self.recordType):
                self.atomName = PDBLine.getAtomName(_rawstring)
                self.x,self.y,self.z = PDBLine.getCoords(_rawstring)
                self.coords = (self.x,self.y,self.z)
                self.occupancy  = PDBLine.getOccupancy(_rawstring)
                self.tempFactor = PDBLine.getTempFactor(_rawstring)
                self.element = PDBLine.getElement(_rawstring)
                self.charge  = PDBLine.getCharge(_rawstring)
        else:
            self.recordType = recordType
            self.serials = list([-1])
            self.residueName = str("")
            self.chainId = str("")
            self.residueNumber = int(-1)
            self.atomName = str("")
            self.x,self.y,self.z = (float(0.0),)*3
            self.coords = (self.x,self.y,self.z)
            self.occupancy = float(1.0)
            self.tempFactor = float(0.0)
            self.element = str("")
            self.charge = str("")

    def __str__(self):
        #care, defaults are hard coded
        ret = ""
        if self.isAtom():
            _tempAtomName = self.atomName
            if len(_tempAtomName)==1:
                _tempAtomName = self.atomName
            ret = "{:6s}{:5d}  {:3s}{:1s}{:3s} {:1s}{:4d}{:1s}   {:8.3f}{:8.3f}{:8.3f}{:6.2f}{:6.2f}           {:2s}{:2s}"\
                .format("ATOM",self.serials[0],_tempAtomName," ",self.residueName,self.chainId,self.residueNumber," ",self.x,self.y,self.z,self.occupancy,self.tempFactor,self.element,self.charge)
        elif self.isConect():
            n = len(self.serials)
            _tempstring  = "{:6s}"+"{:5d}"*n
            ret = _tempstring.format("CONECT",*self.serials)
        elif self.isTer():
            ret = "{:6s}{:5d}      {:3s} {:1s}{:4d}{:1s}".format("TER",self.serials[0],self.residueName,self.chainId,self.residueNumber," ")
        else:
            ret = self._raw
        if ret[-1] != "\n":
            ret += "\n"
        return ret

    def __getattribute__(self,name):
        if name == "coords":
            return (self.x,self.y,self.z)
        else:
            return object.__getattribute__(self,name)

    def __setattr__(self,name,value):
        if name == "coords":
            object.__setattr__(self,name,value)
            self.x,self.y,self.z = value
        else:
            object.__setattr__(self,name,value)

    def getConectSerials(_rawstring):
        iterations = (len(_rawstring)-6)//5
        ret = []
        for i in range(iterations):
            ret.append(int(_rawstring[6+5*i:6+5*i+5]))
        return ret

    def getAtomName(_rawstring):
        if len(_rawstring)>16:
            return _rawstring[12:16].strip()
        else:
            return None
    def getCoords(_rawstring):
        if len(_rawstring)>54:
            try:
                return np.array([float(el) for el in _rawstring[31:54].split()])
            except:
                print("Possibly corrupt pdb line.")
        else:
            return None
    def getOccupancy(_rawstring):
        if len(_rawstring)>60:
            try:
                return float(_rawstring[54:60])
            except:
                print("Possibly corrupt pdb line.")
        else:
            return None
    def getTempFactor(_rawstring):
        if len(_rawstring)>66:
            try:
                return float(_rawstring[60:66])
            except:
                print("Possibly corrupt pdb line.")
        else:
            return None
    def getElement(_rawstring):
        if len(_rawstring)>78:
            return _rawstring[76:78].strip()
        else:
            return None
    def getCharge(_rawstring):
        if len(_rawstring)>80:
            return _rawstring[78:80].strip()
        else:
            return str("")
    def getSerial(_rawstring):
        if len(_rawstring)>12:
            try:
                return int(_rawstring[6:11])
            except:
                print("Possibly corrupt pdb line.")
        else:
            return None
    def setSerial(self,serial):
        if type(serial) == int:
            if serial < 100000:
                self.serial = serial

    def getResidueName(_rawstring):
        if len(_rawstring)>20:
            return _rawstring[17:20]
        else:
            return None
    def getChainId(_rawstring):
        if len(_rawstring)>20:
            return _rawstring[21]
        else:
            return None
    def setChainId(self,chain_id):
        if type(chain_id) == str:
            if len(chain_id) == 1:
                self.chain_id = chain_id

    def getResidueNumber(_rawstring):
        if len(_rawstring)>26:
            try:
                return int(_rawstring[22:26])
            except:
                print("Possibly corrupt pdb line.")
        else:
            return None
    def setResidueNumber(self,residueNumber):
        if type(residueNumber) == int:
            if residueNumber < 10000:
                self.residueNumber = residueNumber
    def getRecordType(_rawstring):
        if len(_rawstring)>7:
            recordstring = _rawstring[0:6]
            if "HEADER" in recordstring:
                return "HEADER"
            elif "HELIX" in recordstring:
                return "HELIX"
            elif "SHEET" in recordstring:
                return "SHEET"
            elif "REMARK" in recordstring:
                return "REMARK"
            elif "HEADER" in recordstring:
                return "HEADER"
            elif "MODEL" in recordstring:
                return "MODEL"
            elif "ENDMDL" in recordstring:
                return "ENDMDL"
            elif "ATOM" in recordstring:
                return "ATOM"
            elif "TER" in recordstring:
                return "TER"
            elif "CONECT" in recordstring:
                return "CONECT"
        else:
            return None
    def headerRecord(recordType):
        return not (PDBLine.strucRecord(recordType) or PDBLine.conectRecord(recordType))
    def strucRecord(recordType):
        return recordType in ["ATOM","TER","HETATM"]
    def terRecord(recordType):
        return recordType in ["TER"]
    def conectRecord(recordType):
        return recordType in ["CONECT"]
    def atomRecord(recordType):
        return recordType in ["ATOM","HETATM"]
    def isTer(self):
        return PDBLine.terRecord(self.recordType)
    def isStruc(self):
        return PDBLine.strucRecord(self.recordType)
    def isAtom(self):
        return PDBLine.atomRecord(self.recordType)
    def isConect(self):
        return PDBLine.conectRecord(self.recordType)
    def isHeader(self):
        return PDBLine.headerRecord(self.recordType)

class PDBFile(object):
    def __init__(self,filename):
        self.fh = open(filename)
        self.raw_lines = [l for l in self.fh]
        self.header = []
        self.structure = []
        self.connectivity = []
        self.pos_d = dict()
        self.splines = dict()
        for i,raw_record in enumerate(self.raw_lines):
            record = PDBLine(i,raw_record)
            if record.isHeader():
                self.header.append(record)
            elif record.isStruc():
                self.structure.append(record)
            elif record.isConect():
                self.connectivity.append(record)

    def __str__(self):
        self.sortConnectivity()
        total = self.header + self.structure + self.connectivity
        return "".join([str(record) for record in total])

    def toFile(self,name):
        fh = open(name,'w')
        fh.write(str(self))
        fh.close()

    def create_pos_d(self):
        pos_d = []
        res_l = []
        n_dict = None
        for i,l in enumerate(self.structure):
            if l.isAtom():
                if not (l.chainId,l.residueNumber) in res_l:
                    if n_dict is not None:
                        pos_d.append(n_dict)
                    n_dict = dict()
                    n_dict['records'] = dict()
                    n_dict['chain'] = l.chainId
                    n_dict['residue'] = l.residueNumber
                    res_l.append((l.chainId,l.residueNumber))
                if 'O3' in l.atomName:
                    n_dict['records']['O3'] = l
                    n_dict['O3'] =  np.array(l.coords)
                elif 'OP1' in l.atomName:
                    n_dict['records']['OP1'] = l
                    n_dict['OP1'] = np.array(l.coords)
                elif 'OP2' in l.atomName:
                    n_dict['records']['OP2'] = l
                    n_dict['OP2'] = np.array(l.coords)
                elif 'P' in l.atomName:
                    n_dict['records']['P'] = l
                    n_dict['P'] = np.array(l.coords)
                elif 'O5' in l.atomName:
                    n_dict['records']['O5'] = l
                    n_dict['O5'] = np.array(l.coords)
                elif 'C5\'' in l.atomName:
                    n_dict['records']['C5'] = l
                    n_dict['C5'] = np.array(l.coords)
                elif 'C4\'' in l.atomName:
                    n_dict['records']['C4'] = l
                    n_dict['C4'] = np.array(l.coords)
                elif 'C3\'' in l.atomName:
                    n_dict['records']['C3'] = l
                    n_dict['C3'] = np.array(l.coords)
        for d in pos_d:
            # if all([k in d for  k in ['P','O3']]):
            #     d['PO3'] = d['O3'] - d['P']
            if all([k in d for  k in ['P','O5']]):
                d['PO5'] = d['O5'] - d['P']
            if all([k in d for  k in ['P','OP1']]):
                d['POP1'] = d['OP1'] - d['P']
            if all([k in d for  k in ['P','OP2']]):
                d['POP2'] = d['OP2'] - d['P']
            # if all([k in d for  k in ['O3','C3']]):
            #     d['C3O3'] = d['C3'] - d['O3']
            if all([k in d for  k in ['O5','C5']]):
                d['C5O5'] = d['C5'] - d['O5']
        for d in pos_d:
            _min_i = -1
            if all([k in d for  k in ['P','O3']]):
                _d = d['O3'] - d['P']
                _min = la.norm(_d)
                for i,e in enumerate(pos_d):
                    if all([k in e for  k in ['O3']]):
                        _e  = e['O3'] - d['P']
                        _mine = la.norm(_e)
                        if _mine < _min:
                            _min_i = i
                            _min = _mine
                            _d = _e
                d['PO3'] = _d
            # if 'O3' in pos_d[_min_i] and 'P' in d:
            #     print(la.norm(pos_d[_min_i]['O3']-d['P']))
            if _min_i >= 0:
                if all([k in pos_d[_min_i] for  k in ['C3','O3']]):
                    d['C3O3'] = pos_d[_min_i]['C3'] - pos_d[_min_i]['O3']
                    d['nC3'] = pos_d[_min_i]['C3']
                    d['nO3'] = pos_d[_min_i]['O3']
                    d['records']['nO3'] = pos_d[_min_i]['records']['O3']
            #         print(la.norm(pos_d[_min_i]['O3']-pos_d[_min_i]['P']))
            # if all([k in d for  k in ['C3','O3']]):
            #     _d = d['O3'] - d['C3']
            #     _min = la.norm(_d)
            #     for e in pos_d:
            #         if all([k in e for  k in ['C3']]):
            #             _e  = e['C3'] - d['O3']
            #             _mine = la.norm(_e)
            #             if _mine < _min:
            #                 _min = _mine
            #                 _d = _e
            #     d['C3O3'] = _d
        self.pos_d = pos_d

    def changeCoordsRecord(self,res_number,atom_name,chain_id,coords):
        for record in self.structure:
            if record.isAtom():
                if record.residueNumber == res_number and record.atomName == atom_name and record.chainId == chain_id:
                    record.x,record.y,record.z = coords
                
    def createStats(self):
        self.create_pos_d()
        angle = lambda a,b: abs(np.dot(a,b))/(la.norm(a)*la.norm(b))
        angles_between = lambda v: [angle(v[i],v[i+1]) for i in range(len(v)-1)]
        to_deg = lambda a: np.arccos(a)/np.pi*180
        angles_between_d = lambda v: [np.arccos(angle(v[i],v[i+1]))/np.pi*180 for i in range(len(v)-1)]
        ortho_comp = lambda u,v:v-np.dot(u,v)*u
        # def reject_outliers(data, m=2):
        #     return data[abs(data - np.mean(data)) < m * np.std(data)]
        def reject_outliers_index(data, m = 2.):
            d = np.abs(data - np.median(data))
            mdev = np.median(d)
            s = d/mdev if mdev else 0.
            i = 0
            ret = []
            for el in s:
                if el<m:
                    ret.append(i)
                i += 1;
            # return data[s<m]
            return ret
        def reject_outliers(data, m = 2.):
            d = np.abs(data - np.median(data))
            mdev = np.median(d)
            s = d/mdev if mdev else 0.
            return data[s<m]
            
        d_PO3 = np.array([la.norm(d['PO3']) for d in self.pos_d if all([k in d for  k in ['P','OP1','OP2','O3','O5']])])
        d_PO5 = np.array([la.norm(d['PO5']) for d in self.pos_d if all([k in d for  k in ['P','OP1','OP2','O3','O5']])])
        d_POP1 = np.array([la.norm(d['POP1']) for d in self.pos_d if all([k in d for  k in ['P','OP1','OP2','O3','O5']])])
        d_POP2 = np.array([la.norm(d['POP2']) for d in self.pos_d if all([k in d for  k in ['P','OP1','OP2','O3','O5']])])
        d_C3O3 = np.array([la.norm(d['C3O3']) for d in self.pos_d if all([k in d for  k in ['P','OP1','OP2','O3','O5']])])
        d_C5O5 = np.array([la.norm(d['C5O5']) for d in self.pos_d if all([k in d for  k in ['P','OP1','OP2','O3','O5']])])        
        a_C5O5P = np.array([angle(d['C5O5'],d['PO5']) for d in self.pos_d if all([k in d for  k in ['P','OP1','OP2','O3','O5','C3','C5']])])
        a_C3O3P = np.array([angle(d['C3O3'],d['PO3']) for d in self.pos_d if all([k in d for  k in ['P','OP1','OP2','O3','O5','C3','C5']])])        
        
        a_O3POP1 = np.array([angle(d['POP1'],d['PO3']) for d in self.pos_d if all([k in d for  k in ['P','OP1','OP2','O3','O5','C3','C5']])])
        a_O5POP1 = np.array([angle(d['POP1'],d['PO5']) for d in self.pos_d if all([k in d for  k in ['P','OP1','OP2','O3','O5','C3','C5']])])
        a_OP2POP1 = np.array([angle(d['POP1'],d['POP2']) for d in self.pos_d if all([k in d for  k in ['P','OP1','OP2','O3','O5','C3','C5']])])
        a_O5POP2 = np.array([angle(d['POP2'],d['PO5']) for d in self.pos_d if all([k in d for  k in ['P','OP1','OP2','O3','O5','C3','C5']])])
        a_O3POP2 = np.array([angle(d['POP2'],d['PO3']) for d in self.pos_d if all([k in d for  k in ['P','OP1','OP2','O3','O5','C3','C5']])])
        a_O5PO3 = np.array([angle(d['PO3'],d['PO5']) for d in self.pos_d if all([k in d for  k in ['P','OP1','OP2','O3','O5','C3','C5']])])

        d_POP1 = reject_outliers(d_POP1,6)
        d_POP2 = reject_outliers(d_POP2,6)
        d_PO5 = reject_outliers(d_PO5,6)
        d_PO3 = reject_outliers(d_PO3,6)
        d_C3O3 = reject_outliers(d_C3O3,6)
        d_C5O5 = reject_outliers(d_C5O5,6)
        a_C5O5P = reject_outliers(a_C5O5P,6)
        a_C3O3P = reject_outliers(a_C3O3P,6)
        
        a_O3POP1_ids = reject_outliers_index(a_O3POP1,6)
        a_O3POP2_ids = reject_outliers_index(a_O3POP2,6)
        a_O5POP1_ids = reject_outliers_index(a_O5POP1,6)
        a_O5POP2_ids = reject_outliers_index(a_O5POP2,6)
        a_OP2POP1_ids = reject_outliers_index(a_OP2POP1,6)
        a_O5PO3_ids = reject_outliers_index(a_O5PO3,6)


        print(len(a_O3POP1_ids))
        no_outliers = set(a_O3POP1_ids)
        no_outliers = no_outliers.intersection(set(a_O3POP2_ids))
        no_outliers = no_outliers.intersection(set(a_O3POP2_ids))
        no_outliers = no_outliers.intersection(set(a_O5POP1_ids))
        no_outliers = no_outliers.intersection(set(a_O5POP2_ids))
        no_outliers = no_outliers.intersection(set(a_OP2POP1_ids))
        no_outliers = list(no_outliers.intersection(set(a_O5PO3_ids)))
        print(len(no_outliers))
        # plt.plot(no_outliers)
        # plt.show()

        m_a_O3POP1 = np.mean(a_O3POP1)
        m_a_O3POP2 = np.mean(a_O3POP2)
        m_a_O5POP1 = np.mean(a_O5POP1)
        m_a_O5POP2 = np.mean(a_O5POP2)
        m_a_OP1POP2 = np.mean(a_OP2POP1)
        m_a_O5PO3 = np.mean(a_O5PO3)
        m_a_C5O5P = np.mean(a_C5O5P)
        m_a_C3O3P = np.mean(a_C3O3P)

        print("m_a_O3POP1,m_a_O5POP1,m_a_OP1POP2",\
            to_deg(m_a_O3POP1),to_deg(m_a_O5POP1),to_deg(m_a_OP1POP2))
        # v_a_O3POP1 = np.var(a_O3POP1)
        # v_a_O3POP2 = np.var(a_O3POP2)
        # v_a_O5POP1 = np.var(a_O5POP1)
        # v_a_O5POP2 = np.var(a_O5POP2)
        # v_a_OP1POP2 = np.var(a_OP2POP1)
        # v_a_O5PO3 = np.var(a_O5PO3)

        
        # m_a_C5O5P = np.mean(a_C5O5P)
        # m_a_C3O3P = np.mean(a_C3O3P)
        m_PO3 = np.mean(d_PO3)
        m_PO5 = np.mean(d_PO5)
        m_POP1 = np.mean(d_POP1)
        m_POP2 = np.mean(d_POP2)
        m_C3O3 = np.mean(d_C3O3)
        m_C5O5 = np.mean(d_C5O5)
        # #correct length of P-OP1 and P-OP2
        # for d in self.pos_d:
        #     if (d['chain'] in ['K','E','I','G'] and d['residue'] == -4):
        #         new_pos_OP1 = d['P'] + m_POP1/la.norm(d['POP1'])*d['POP1']
        #         new_pos_OP2 = d['P'] + m_POP1/la.norm(d['POP2'])*d['POP2']
        #         d['records']['OP1'].coords = new_pos_OP1
        #         d['records']['OP2'].coords = new_pos_OP2

        d_PGroups = np.array([d for i,d in enumerate(self.pos_d) if all([k in d for  k in ['P','OP1','OP2','nO3','O5']]) and i in no_outliers])
        d_PPos = np.array([[d[k] for k in ['P','OP1','OP2','nO3','O5']] for d in d_PGroups])
        d_aligned = [center(d_PPos[0])]
        for d in d_PPos[1:]:
            d_aligned.append(align(d_PPos[0],d))
            print(d_aligned[-1])
        av_pos = np.array([[0.0,0.0,0.0],[0.0,0.0,0.0],[0.0,0.0,0.0],[0.0,0.0,0.0]])
        for d in d_aligned:
            av_pos[0] += d[1]
            av_pos[1] += d[2]
            av_pos[2] += d[3]
            av_pos[3] += d[4]
     
        av_pos[0] = np.array([np.sqrt(8/9),0,-1/3])
        av_pos[1] = np.array([-np.sqrt(2/9),np.sqrt(2/3),-1/3])
        av_pos[2] = np.array([-np.sqrt(2/9),-np.sqrt(2/3),-1/3])
        av_pos[3] = np.array([0,0,1])

        av_pos[0] /= la.norm(av_pos[0])
        av_pos[1] /= la.norm(av_pos[1])
        av_pos[2] /= la.norm(av_pos[2])
        av_pos[3] /= la.norm(av_pos[3])

        av_pos[0] *= m_POP1
        av_pos[1] *= m_POP2
        av_pos[2] *= m_PO3
        av_pos[3] *= m_PO5

        print(to_deg(angle(av_pos[2],av_pos[1])),to_deg(angle(av_pos[3],av_pos[1])),\
            to_deg(angle(av_pos[0],av_pos[1])))

        # print(av_pos)
        # print(m_POP1,m_POP2,m_PO3,m_PO5)

        # print(av_pos)
        # print(np.average(d_aligned))
        # print(len(d_PPos))

        atom_order = ['P','OP1','OP2','nO3','O5']
        # print(m_PO3,m_PO5,m_POP1,m_POP2,m_C3O3,to_deg(m_a_O3POP1),to_deg(m_a_C5O5P),to_deg(m_a_C3O3P))
        for d in self.pos_d:
            if (d['chain'] in ['K','E','I','G'] and d['residue'] == -4):
                #O3 and C3 are in the next residue
                C3 = d['nC3']
                C5 = d['C5']
                #shift coordinates
                P = d['P']
                print(la.norm(P-C3))
                print(la.norm(P-C5))
                print(la.norm(C3-C5))

                OP1 = av_pos[0] 
                OP2 = av_pos[1] 
                O3 = av_pos[2] 
                O5 = av_pos[3] 

                R_u = lambda u: R.from_quat((np.sqrt(abs(1-u[0]))*np.sin(2*np.pi*u[1]),\
                    np.sqrt(abs(1-u[0]))*np.cos(2*np.pi*u[1]),np.sqrt(abs(u[0]))*\
                    np.sin(2*np.pi*u[2]),np.sqrt(abs(u[0]))*np.cos(2*np.pi*u[2])))
                
                op1_v_u = lambda u: R_u(u).apply(OP1)
                op2_v_u = lambda u: R_u(u).apply(OP2)
                o3_v_u = lambda u: R_u(u).apply(O3)
                o5_v_u = lambda u: R_u(u).apply(O5)
                a_3_u = lambda u: abs(angle(o3_v_u(u),o3_v_u(u)+P-C3))
                a_5_u = lambda u: abs(angle(o5_v_u(u),o5_v_u(u)+P-C5))

                d_u = lambda u: abs(la.norm(o3_v_u(u) + P - C3)- m_C3O3) \
                + abs(la.norm(o5_v_u(u) + P - C5) - m_C5O5)\
                + abs(a_5_u(u)-m_a_C5O5P) + abs(a_3_u(u)-m_a_C3O3P)
                con_u = opt.LinearConstraint(np.eye(3,3),0,1)
                res = opt.minimize(d_u,[0.5,0.5,0.5],constraints=[con_u],method='trust-constr')
                print("=======")
                print(res.x)
                print(d_u(res.x))

                OP1 = op1_v_u(res.x)+P
                OP2 = op2_v_u(res.x)+P
                O3 = o3_v_u(res.x)+P
                O5 = o5_v_u(res.x)+P

                nO3 = d['records']['nO3']
                nO3.coords = O3
                d['records']['O5'].coords = O5
                d['records']['OP1'].coords = OP1
                d['records']['OP2'].coords = OP2

                # c_u = lambda u1,u2,u3: la.norm(o3_v_u(u1,u2,u3)-o5_v_u(u1,u2,u3))
                # print(d_u(0,0,0))
                # OP1 = d['OP1']
                # POP1 = d['POP1']
                #O3 == x
                #angle(C3,O3,P)
                #angle(O3,P,OP1)
                #distance(C3,O3)
                #distance(P,O3)
                # df_C3O3 = lambda x: np.abs(la.norm(C3-x) - m_C3O3) 
                # df_PO3 = lambda x: np.abs(la.norm(P-x) - m_PO3)
                # af_C3O3P = lambda x: np.abs(angle(P-x,C3-x) - m_a_C3O3P)
                # af_O3POP1 = lambda x:np.abs(angle(P-x,POP1) - m_a_O3POP1)
                # func = lambda x: df_C3O3(x)+df_PO3(x)+af_O3POP1(x)+af_C3O3P(x)
                # x0 = d['nO3']
                # res = opt.minimize(func,x0,method='Nelder-Mead')
                # nO3 = d['records']['nO3']
                # nO3.coords = res.x
                # print(res.x)
                # print(nO3.chainId,nO3.residueNumber)
                # print(df_C3O3(res.x),m_C3O3)
                # print(df_PO3(res.x),m_PO3)
                # print(af_C3O3P(res.x),m_a_C3O3P)
                # print(af_O3POP1(res.x),m_a_O3POP1)

                # df_C5O5 = lambda x: np.abs(la.norm(C5-x) - m_C5O5) 
                # df_PO5 = lambda x: np.abs(la.norm(P-x) - m_PO5)
                # af_C5O5P = lambda x: np.abs(angle(P-x,C5-x) - m_a_C5O5P)
                # af_O5POP1 = lambda x:np.abs(angle(P-x,POP1) - m_a_O5POP1)
                # func = lambda x: df_C5O5(x)+df_PO5(x)+af_O5POP1(x)+af_C5O5P(x)
                # x0 = d['O5']
                # res = opt.minimize(func,x0,method='Nelder-Mead')
                # nO5 = d['records']['O5']
                # nO5.coords = res.x
                # print(res.x)
                # print(nO5.chainId,nO5.residueNumber)
                # print(df_C5O5(res.x),m_C5O5)
                # print(df_PO5(res.x),m_PO5)
                # print(af_C5O5P(res.x),m_a_C5O5P)
                # print(af_O5POP1(res.x),m_a_O5POP1)

            # lengths = np.unique([len(d.keys()) for d in pos])
            # max_length = max(lengths)
            # pos_complete = [d for d in pos if len(d) == max_length]
            # spline = pdb.produce_splines(k1,k2)
            # x_d_s,values,_eval = self.get_x_d_s(pos_complete)
            
            # #c_PO3C3 = [np.cross(d['CO3'],d['PO3']) for d in pos_complete if all([k in d for  k in ['P','OP1','OP2','O3','O5','C3','C5']])]
            # o1_rotors = []
            # o2_rotors = []
            # for x,d in zip(x_d_s,pos_complete):
            #     tangent = spline['Pspline'].derivative()(x)
            #     tangent /= la.norm(tangent)
            #     o1_rotors.append(ortho_comp(tangent,d['POP1']))
            #     o2_rotors.append(ortho_comp(tangent,d['POP2']))
                
            # print(list(zip(angles_between_d(o1_rotors),angles_between_d(o2_rotors))))
        #c_r = [(d['chain'],d['residue']) for d in self.pos_d if all([k in d for  k in ['P','OP1','OP2','O3','O5']])]

         #a_O1PO2 = [angle(d['POP1'],d['POP2']) for d in self.pos_d if all([k in d for  k in ['P','OP1','OP2','O3','O5','C3','C5']])] 
         
        #a_O1PO5 = [angle(d['POP1'],d['PO5']) for d in self.pos_d if all([k in d for  k in ['P','OP1','OP2','O3','O5','C3','C5']])] 
        #print(len(c_PO3C3))
        #print(len(p_data['KFx_d_s']))
        #print(len(
        #print(a_O1PO2)
        #print(a_O1PO3)
        #print(a_O1PO5)
        #plt.plot(a_O1PO2)
        #plt.show()

    def append_chains(self,k1,k2):
        ret = []
        self.create_pos_d()
        ret.extend([el for el in self.pos_d if el['chain']==k1])
        ret.extend([el for el in self.pos_d if el['chain']==k2])
        return ret

    def getTerIndex(self,k1):
        ret = [(i,record) for i,record in enumerate(self.structure) if record.isTer()]
        ret = [ i for i,record in ret if record.chainId == k1]
        if ret:
            return ret[0]
        else:
            return None

    def sortConnectivity(self):
        self.connectivity.sort(key=lambda record: record.serials[0])

    def concatenateChains(self,k1,k2):
        #get residues of respective chains, serials
        k1_records = [record for record in self.structure if record.chainId == k1]
        k2_records = [record for record in self.structure if record.chainId == k2]
        rest_struc_records = [record for record in self.structure if not (record in k1_records or record in k2_records)]
        res_shift = -k2_records[0].residueNumber + k1_records[-1].residueNumber + 1
        #reassign them
        for record in k2_records:
            record.chainId = k1
            record.residueNumber += res_shift
        self.structure = k1_records[:-1] + k2_records + rest_struc_records
        self.smoothenSerials()

    def getSerialIndex(self,serial):
        for record in self.structure:
            if record.serials[0] == serial:
                return self.structure.index(record)
        return -1

    def addRecordsAfterSerial(self,serial,records):
        i = self.getSerialIndex(serial)
        for record in records:
            self.structure.insert(i,record)
            i += 1
        self.smoothenSerials()
    
    def removeRecordsAfterSerial(self,serial,n):
        for i in range(serial,serial+n+1):
            for record in self.structure:
                 if i == record.serials[0]:
                     self.structure.remove(record)
          
    def smoothenSerials(self):
        s = 1
        serial_map = dict()
        for record in self.structure:
            s_record = record.serials[0]
            if s_record != s:
                record.serials[0] = s
                serial_map[s_record] = s
            s+=1
        for record in self.connectivity:
            for i,s_record in enumerate(record.serials):
                if s_record in serial_map:
                    record.serials[i] = serial_map[s_record]

    def get_x_d_s(self,pos_p,k='P'):
        x = []
        x_d = []
        data = []
        pos_p = [d for d in pos_p if k in d]
        for i in range(len(pos_p)):
            if k in pos_p[i]:
                x.append(pos_p[i][k])
        for i in range(len(x)-1):
            d = math.sqrt(sum([(e1-e0)**2 for e0,e1 in zip(x[i],x[i+1])]))
            x_d.append(d)
            data.append(x[i])
        x_d_s = [sum(x_d[:i]) for i in range(0,len(x_d)+1)]
        data.append(pos_p[-1][k])
        eval_x = sum(x_d_s[x_d.index(max(x_d)):x_d.index(max(x_d))+2])/2
        return x_d_s,data,eval_x

    def checkSerialContigency(self):
        serials = []
        struc_lines = []
        for line in self.structure:
            serials.append(line.serials[0])
            struc_lines.append(line)
        for i in range(len(serials)-1):
            s_i = serials[i]
            s_i_1 = serials[i+1]
            d = s_i_1-s_i
            if d != 1:
                print(d,"|",struc_lines[i])

    def produce_splines(self,k1,k2):
        spline_dict = dict()
        pos = self.append_chains(k1,k2)
        pos_len = [len(d) for d in pos]
        min_id = pos_len.index(min(pos_len[:-1]),1,len(pos_len))
        lack_res = pos[min_id]
        spline_dict['chain'],spline_dict['residue'] = lack_res['chain'],lack_res['residue']
        spline_dict['other_chain'] = k1 if spline_dict['chain']==k2 else k2
        for atomName in ['P','OP1','OP2','O5','O3']:
            x,y,eval_x = self.get_x_d_s(pos,atomName)
            spline_dict[atomName+'spline'] = CubicSpline(x,y)
            spline_dict[atomName] = spline_dict[atomName+'spline'](eval_x)
            self.splines[k1+k2+atomName] = spline_dict[atomName] 
        return spline_dict

    def findResidueEdge(self,chainId,residueNumber,start_stop):
        in_chain = False
        for i,record in enumerate(self.structure):
            if not in_chain:
                if not (hasattr(record,'chainId') and hasattr(record,'serials')):
                    continue
                if not (record.chainId == chainId and record.residueNumber == residueNumber):
                    continue
                in_chain = True
                if start_stop:
                    return record.serials[0]
            else:
                if record.isTer() or record.residueNumber != residueNumber or record.chainId != chainId:
                    return self.structure[i-1].serials[0]

    def printConectRecords(self):
        for c_record in self.connectivity:
            connected_records = [self.lines[self.getSerialIndex(serial)] for serial in c_record.serials]
            connected_names = [record.atomName for record in connected_records]
            connected_chains = [record.chainId for record in connected_records]
            connected_residues = [record.residueNumber for record in connected_records]
            connect_info = ["%s-%s-%i"%(name,chain,res) for name,chain,res in zip(connected_names,connected_chains,connected_residues)]
            temp_str = "{:10s} -- ["+"{:10s} |"*(len(connected_records)-1)
            temp_str = temp_str[:-1]+"]"
            print(temp_str.format(*connect_info))
  # scn = bpy.context.scene

# coord_E_J = [ 87.86136708, 106.1081981 , 101.58814473]
# coord_F_K = [109.28392868,  88.83227938, 100.98471716]
# coord_I_H = [109.04143043, 129.57377542, 100.63968791]
# coord_G_L = [130.31922946, 112.30710638, 101.43790934]


# extra_coords = [coord_E_J,coord_F_K,coord_I_H,coord_G_L]
# extra_vs = [[ 0.04988128,  0.90259269, -1.50955993],
#             [-1.19390859, -0.57616617, -1.14431159],
#             [ 1.26297609,  0.55373933, -1.10137291],
#             [ 0.08339191, -0.923248  , -1.46801009]]

# fn = '/home/kkarius/workingdir/Jiri Holliday/HJ_DNA_Long_raw.pdb'
# pdbfile = PDBFile(fn_alt)


if __name__ == "__main__":
    import sys
    fn = sys.argv[1]
    pdb = PDBFile(fn)
    pdb.removeRecordsAfterSerial(2297,9)
    pdb.removeRecordsAfterSerial(985,9)
    pdb.removeRecordsAfterSerial(331,9)
    pdb.removeRecordsAfterSerial(1643,9)
    pdb.smoothenSerials()
    pdb.toFile('./updated_HJ_less_base.pdb')
    sys.exit(0)
    fn = './HJ_DNA_Long_rawer.pdb'
    fn_alt = '/Users/Kai/git/pdb_utils/kai/HJ_DNA_Long_rawer.pdb'
    pdb = PDBFile(fn_alt)
    #pdb.removeRecord(-3,'O5\'')
    #pdb.removeRecord(-5,'O3\'')
    # print(pdb.findResidueEdge("E",-5,True))
    # print(pdb.findResidueEdge("E",-6,False))

    # print(['P' in d.keys() for d in pdb.create_pos_d()])
    splines = [pdb.produce_splines('K','F'),pdb.produce_splines('E','J'),pdb.produce_splines('I','H'),pdb.produce_splines('G','L')]
    for spl in splines:
        chain_id = spl['chain']
        other_chain = spl['other_chain']
        residue_number = spl['residue']
        serial_number = pdb.findResidueEdge(chain_id,residue_number,True)
        serial_number_o3 = pdb.findResidueEdge(other_chain,-5,False)
        n_records = []
        for atomName,element in zip(["P","OP1","OP2"],["P","O","O"]):
            line = PDBLine(recordType="ATOM")
            line.atomName = atomName
            line.residueName = "DT"
            line.residueNumber = residue_number
            line.element = element
            line.chainId = chain_id
            line.x,line.y,line.z = spl[atomName]
            n_records.append(line)
        pdb.addRecordsAfterSerial(serial_number,n_records)
        # pdb.changeCoordsRecord(-5,"O3",other_chain,spl["O3"])
        # pdb.changeCoordsRecord(-3,"O5",chain_id,spl["O5"])

    # pdb.printConectRecords()
        # print(pdb.lines)
    pdb.concatenateChains('K','F')
    pdb.concatenateChains('E','J')
    pdb.concatenateChains('I','H')
    pdb.concatenateChains('G','L')
    pdb.checkSerialContigency()
    pdb.createStats()
    pdb.toFile('/Users/Kai/git/pdb_utils/kai/test_out_raw.pdb')
    
# gs = []
# lines = [l for l in fh]

# pos_d = get_pos_d(lines)
# o3s = [get_coords(l) for l in lines if is_atomic(l) and 'O3' in get_spec(l)]

# geo_d = []
# for d in pos_d:
#     if 'P' in d:
#         for o in o3s:
#             if n(d['P'] - o)<2:
#                 d['O3'] = o
#                 geo_d.append(d)

# print(len(geo_d))

# produce_splines(geo_d,'E','J')
#for i,d in enumerate(geo_d):
#    print("Processing object nr:",i)
#    for k in d.keys():
#        if k == 'O3':
#            sphere(d[k],0.5,'red')
#        elif k in ['O5','OP1','OP2']:
#            sphere(d[k],0.5,'yellow')
#        elif k == 'P':
#            sphere(d[k],1,'green')
#        

#chain_dict = defaultdict(list)
#for pl in plines:
#    chain_dict[get_chain(pl)].append((get_residue(pl),get_coords(pl)))

#chain_to_mat = dict()

#cc = 0
#for k in chain_dict.keys():
#    chain_to_mat[k] = mat_dict[colors[cc]]
#    cc+=1

#chain_to_mat["F"] = chain_to_mat["K"]
#chain_to_mat["I"] = chain_to_mat["H"]
#chain_to_mat["E"] = chain_to_mat["J"]
#chain_to_mat["G"] = chain_to_mat["L"]
#            


#for i in range(len(vs)):
#    n_arrow = arrow.copy()
#    n_arrow.data = arrow.data.copy()
#    n_arrow.animation_data_clear()
#    bpy.context.collection.objects.link(n_arrow)
#    n_arrow.location = tuple(posn[i][0])
#    n_arrow.rotation_euler = (alpha(vs[i][0]),0,beta(vs[i][0]))
#    
#for i in range(len(extra_vs)):
#    n_arrow = arrow.copy()
#    n_arrow.data = arrow.data.copy()
#    n_arrow.animation_data_clear()
#    bpy.context.collection.objects.link(n_arrow)
#    n_arrow.location = tuple(extra_coords[i])
#    n_arrow.active_material = mat_dict["green"]
#    n_arrow.rotation_euler = (alpha(extra_vs[i]),0,beta(extra_vs[i]))

#for e_c in extra_coords:
#    bpy.ops.mesh.primitive_uv_sphere_add(radius=1,location=tuple(e_c))
#    bpy.context.selected_objects[0].active_material = mat_dict["green"]
