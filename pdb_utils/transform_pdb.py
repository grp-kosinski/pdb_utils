import numpy
import math

def is_coord_line(line):
    '''
    Return True if the passed PDB line contains coordinates according to PDB format spec
    '''
    return line[0:6] in ('ATOM  ', 'HETATM', 'ANISOU')


def transform_coordinates(all_coords, rot, trans):
    rot = numpy.array(rot, dtype = float)
    trans = numpy.array(trans, dtype = float)
    out = []
    for x, y, z in all_coords:
        coord = numpy.array((x, y, z), "f")

        coord = numpy.dot(coord, rot)+trans
        out.append(coord)

    return out

def transform(pdbfilename, rot, trans, trans_first=False):
    '''
    Transform PDB file with rotation and translation matrix
    without changing anything in the file but coordinates.

    rot - 3x3 list of lists or numpy array e.g.:
      [[-0.037084, -0.983558, 0.176743],[-0.023889, 0.177686, 0.983797],[-0.999027, 0.032261, -0.030086]]
    trans - list of len 3 or numpy array , e.g.:
      [0.090, 67.385, -2.190]

    Return string with new transformed PDB
    '''
    rot = numpy.array(rot, dtype = float)
    trans = numpy.array(trans, dtype = float)

    outlines = []
    with open(pdbfilename) as f:
        for line in f:
            if is_coord_line(line):
                if not line.startswith('ANISOU'):
                    x = line[30:38]
                    y = line[38:46]
                    z = line[46:54]
                    coord = numpy.array((x, y, z), "f")

                    if trans_first:
                        coord = trans + numpy.dot(coord, rot)
                    else:
                        coord = numpy.dot(coord, rot)+trans

                    new_x = '{1:8.{0}f}'.format(2 if coord[0]<0 else 3,coord[0])
                    new_y = '{1:8.{0}f}'.format(2 if coord[1]<0 else 3,coord[1])
                    new_z = '{1:8.{0}f}'.format(2 if coord[2]<0 else 3,coord[2])
                    line = line[:30] + new_x + new_y + new_z + line[54:]

            outlines.append(line)

    return ''.join(outlines)

def transform_pdblines(pdblines, rot, trans, trans_first=False):
    '''
    Transform PDB lines with rotation and translation matrix
    without changing anything in the file but coordinates.

    rot - 3x3 list of lists or numpy array e.g.:
      [[-0.037084, -0.983558, 0.176743],[-0.023889, 0.177686, 0.983797],[-0.999027, 0.032261, -0.030086]]
    trans - list of len 3 or numpy array , e.g.:
      [0.090, 67.385, -2.190]

    Return string with new transformed PDB
    '''
    rot = numpy.array(rot, dtype = float)
    trans = numpy.array(trans, dtype = float)

    outlines = []
    for line in pdblines:
        if is_coord_line(line):
            x = line[30:38]
            y = line[38:46]
            z = line[46:54]
            coord = numpy.array((x, y, z), "f")

            if trans_first:
                coord = trans + numpy.dot(coord, rot)
            else:
                coord = numpy.dot(coord, rot)+trans

            new_x = '{1:8.{0}f}'.format(2 if coord[0]<0 else 3,coord[0])
            new_y = '{1:8.{0}f}'.format(2 if coord[1]<0 else 3,coord[1])
            new_z = '{1:8.{0}f}'.format(2 if coord[2]<0 else 3,coord[2])
            line = line[:30] + new_x + new_y + new_z + line[54:]

        outlines.append(line)

    return ''.join(outlines)



def quaternion_to_matrix(quaternion):
    '''
    Modified from http://www.flipcode.com/documents/matrfaq.html#Q54

    Tested that it works for quaternions from IMP
    '''
    W, X, Y, Z = quaternion
    xx      = X * X;
    xy      = X * Y;
    xz      = X * Z;
    xw      = X * W;

    yy      = Y * Y;
    yz      = Y * Z;
    yw      = Y * W;

    zz      = Z * Z;
    zw      = Z * W;

    return [
        [1 - 2 * ( yy + zz ), 2 * ( xy + zw ), 2 * ( xz - yw )],
        [2 * ( xy - zw ), 1 - 2 * ( xx + zz ), 2 * ( yz + xw )],
        [2 * ( xz + yw ), 2 * ( yz - xw ), 1 - 2 * ( xx + yy )]
    ]


def euler_to_matrix(psi, theta, phi, in_degrees=False):
    if in_degrees:
        rot_conv = 3.14159265358979323846/180
        psi = psi * rot_conv
        theta = theta * rot_conv
        phi = phi * rot_conv
    # print psi, theta, phi
    sin_psi = math.sin( psi );
    cos_psi = math.cos( psi );
    sin_theta = math.sin( theta );
    cos_theta = math.cos( theta );
    sin_phi = math.sin( phi);
    cos_phi = math.cos( phi );

    a = cos_psi * cos_phi - cos_theta * sin_phi * sin_psi
    b = cos_psi * sin_phi + cos_theta * cos_phi * sin_psi
    c = sin_psi * sin_theta
    d = -sin_psi * cos_phi- cos_theta * sin_phi * cos_psi
    e = -sin_psi * sin_phi+ cos_theta * cos_phi * cos_psi
    f =cos_psi * sin_theta
    g =sin_theta * sin_phi
    h = -sin_theta * cos_phi
    j =cos_theta

    # return [
    #     [a, b, c],
    #     [d, e, f],
    #     [g, h, j]
    # ]
    return [
        [a, d, g],
        [b, e, h],
        [c, f, j]
    ]


# if __name__ == '__main__':
#     pdbfilename = sys.argv[1]
#     with open(sys.argv[2]) 
#     rot = 
#     trans = sys.argv[3]
#     transform(pdbfilename, rot, trans)

def rotation_from_matrix(matrix):
    """Return rotation angle and axis from rotation matrix.

    >>> angle = (random.random() - 0.5) * (2*math.pi)
    >>> direc = numpy.random.random(3) - 0.5
    >>> point = numpy.random.random(3) - 0.5
    >>> R0 = rotation_matrix(angle, direc, point)
    >>> angle, direc, point = rotation_from_matrix(R0)
    >>> R1 = rotation_matrix(angle, direc, point)
    >>> is_same_transform(R0, R1)
    True

    """
    R = numpy.array(matrix, dtype=numpy.float64, copy=False)
    R33 = R[:3, :3]
    # direction: unit eigenvector of R33 corresponding to eigenvalue of 1
    w, W = numpy.linalg.eig(R33.T)
    i = numpy.where(abs(numpy.real(w) - 1.0) < 1e-8)[0]
    if not len(i):
        raise ValueError("no unit eigenvector corresponding to eigenvalue 1")
    direction = numpy.real(W[:, i[-1]]).squeeze()
    # point: unit eigenvector of R33 corresponding to eigenvalue of 1
    w, Q = numpy.linalg.eig(R)
    i = numpy.where(abs(numpy.real(w) - 1.0) < 1e-8)[0]
    if not len(i):
        raise ValueError("no unit eigenvector corresponding to eigenvalue 1")
    point = numpy.real(Q[:, i[-1]]).squeeze()
    point /= point[3]
    # rotation angle depending on direction
    cosa = (numpy.trace(R33) - 1.0) / 2.0
    if abs(direction[2]) > 1e-8:
        sina = (R[1, 0] + (cosa-1.0)*direction[0]*direction[1]) / direction[2]
    elif abs(direction[1]) > 1e-8:
        sina = (R[0, 2] + (cosa-1.0)*direction[0]*direction[2]) / direction[1]
    else:
        sina = (R[2, 1] + (cosa-1.0)*direction[1]*direction[2]) / direction[0]
    angle = math.atan2(sina, cosa)
    return angle, direction, point
